// Copyright 2011 INDILINX Co., Ltd.
//
// This file is part of Jasmine.
//
// Jasmine is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Jasmine is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Jasmine. See the file COPYING.
// If not, see <http://www.gnu.org/licenses/>.
//
// GreedyFTL source file
//
// Author; Sang-Phil Lim (SKKU VLDB Lab.)
//
// - support POR
//  + fixed metadata area (Misc. block/Map block)
//  + logging entire FTL metadata when each ATA commands(idle/ready/standby) was issued
//

#include "jasmine.h"

//----------------------------------
// macro
//----------------------------------
#define VC_MAX              0xCDCD
#define MISCBLK_VBN         0x1 // vblock #1 <- misc metadata
#define MAPBLKS_PER_BANK    (((PAGE_MAP_BYTES / NUM_BANKS) + BYTES_PER_PAGE - 1) / BYTES_PER_PAGE)
//lfm : ogh FIXME
#define META_BLKS_PER_BANK  (1 + 1 + MAPBLKS_PER_BANK + 1) // include block #0, misc block

// the number of sectors of misc. metadata info.
#define NUM_MISC_META_SECT  ((sizeof(misc_metadata) + BYTES_PER_SECTOR - 1)/ BYTES_PER_SECTOR)
#define NUM_VCOUNT_SECT     ((VBLKS_PER_BANK * sizeof(UINT16) + BYTES_PER_SECTOR - 1) / BYTES_PER_SECTOR)

//----------------------------------
// metadata structure
//----------------------------------
typedef struct _ftl_statistics
{
    UINT32 gc_cnt;
    UINT32 page_wcount; // page write count
}ftl_statistics;

typedef struct _misc_metadata
{
    UINT32 cur_write_vpn; // physical page for new write
    UINT32 cur_miscblk_vpn; // current write vpn for logging the misc. metadata
    UINT32 cur_mapblk_vpn[MAPBLKS_PER_BANK]; // current write vpn for logging the age mapping info.
    UINT32 gc_vblock; // vblock number for garbage collection
    UINT32 free_blk_cnt; // total number of free block count
    UINT32 lpn_list_of_cur_vblock[PAGES_PER_BLK]; // logging lpn list of current write vblock for GC

    // lfm : ogh FIXME
    UINT32 start_pos;
    UINT32 end_pos;
    UINT32 num_entry;
    UINT32 total_size; 
// ogh : lfm FIXME
    UINT32 cur_logblk_vpn;
    UINT32 cur_logblk_bank;
}misc_metadata; // per bank

// ogh : lfm FIXME 
#define LOGBLK_VBN	0x2
#define NUM_SUB_PMAP_SECT	((BYTES_PER_PAGE + BYTES_PER_SECTOR -1 ) / BYTES_PER_SECTOR )
#define	get_cur_logblk_bank()	(g_misc_meta[0].cur_logblk_bank)
#define	get_logblk_vpn(bank)	(g_misc_meta[bank].cur_logblk_vpn)
#define	set_logblk_vpn(bank,vpn)	(g_misc_meta[bank].cur_logblk_vpn=vpn)
#define inc_logblk_vpn(bank)		(g_misc_meta[bank].cur_logblk_vpn++)
#define inc_logblk_bank()	(g_misc_meta[0].cur_logblk_bank = (g_misc_meta[0].cur_logblk_bank +1 )%NUM_BANKS)


#define align_to_sector(size) ((size + BYTES_PER_SECTOR -1 ) / BYTES_PER_SECTOR * BYTES_PER_SECTOR)
#define align_to_dram(size)	((size + DRAM_ECC_UNIT -1 ) / DRAM_ECC_UNIT * DRAM_ECC_UNIT )
static void logging_sub_pmap_table(void);
static void load_sub_pmap_table(void);
static void write_to_sata(char *addr, UINT32 size);
static void read_sata_buffer(UINT32 idx);
static UINT8 get_file_idx(UINT8 id);
static void send_ack(UINT8 id, UINT16 size, UINT32 error);
static void copy_data (UINT32 dest, UINT32 src, UINT32 size );
static void lfm_garbage_collection(void);
// command queue 
static UINT8  _id;
//static UINT8 _cmd, _id;
static UINT16 _size;
static char _name[MAX_FILE_NAME];
static UINT32 ret_size;
static UINT32 _data;
static UINT32 _req_size;
static UINT32 _flag;
static UINT32 _sectors;
static UINT32 	max_logging_rate= MAX_LOGGING_RATE;
static UINT32	max_peak_rate_duration	= MAX_PEAK_RATE_DURATION;

#ifndef ID_BYTES 
#define ID_BYTES 1
#endif 

static UINT32 end_pos[MAX_NUM_FILE];
static UINT32 cur_pos[MAX_NUM_FILE];
static UINT32 cur_bytes[MAX_NUM_FILE];
static UINT32 prev_size[MAX_NUM_FILE];
static UINT32 total_size[MAX_NUM_FILE];
static UINT32 actual_size[MAX_NUM_FILE];
static UINT32 mark_pos[MAX_NUM_FILE];

/* JLEE */
#define	LFM_INIT_POS	(SECTORS_PER_PAGE * 128)
//#define	LFM_END_POS	(NUM_LPAGES * SECTORS_PER_PAGE)
#define	LFM_END_POS	(SECTORS_PER_PAGE * 192)	// for test
#define	LFM_RETAIN_SIZE	(BYTES_PER_PAGE * 8)

#define NO_FORMAT	1
/* END JLEE */

// lfm ]] end 

//----------------------------------
// FTL metadata (maintain in SRAM)
//----------------------------------
static misc_metadata  g_misc_meta[NUM_BANKS];
static ftl_statistics g_ftl_statistics[NUM_BANKS];
static UINT32		  g_bad_blk_count[NUM_BANKS];

// SATA read/write buffer pointer id
UINT32 				  g_ftl_read_buf_id;
UINT32 				  g_ftl_write_buf_id;

//----------------------------------
// NAND layout
//----------------------------------
// block #0: scan list, firmware binary image, etc.
// block #1: FTL misc. metadata
// block #2 ~ #31: page mapping table
// block #32: a free block for gc
// block #33~: user data blocks

//----------------------------------
// macro functions
//----------------------------------
#define is_full_all_blks(bank)  (g_misc_meta[bank].free_blk_cnt == 1)
#define inc_full_blk_cnt(bank)  (g_misc_meta[bank].free_blk_cnt--)
#define dec_full_blk_cnt(bank)  (g_misc_meta[bank].free_blk_cnt++)
#define inc_mapblk_vpn(bank, mapblk_lbn)    (g_misc_meta[bank].cur_mapblk_vpn[mapblk_lbn]++)
#define inc_miscblk_vpn(bank)               (g_misc_meta[bank].cur_miscblk_vpn++)

// page-level striping technique (I/O parallelism)

//#define get_num_bank(lpn)             ((lpn) % NUM_BANKS)
//lfm : ogh FIXME 
#define get_num_bank(lpn)             ((lpn) / (PAGES_PER_BANK - (META_BLKS_PER_BANK * PAGES_PER_BLK))) 
#define get_bad_blk_cnt(bank)         (g_bad_blk_count[bank])
#define get_cur_write_vpn(bank)       (g_misc_meta[bank].cur_write_vpn)
#define set_new_write_vpn(bank, vpn)  (g_misc_meta[bank].cur_write_vpn = vpn)
#define get_gc_vblock(bank)           (g_misc_meta[bank].gc_vblock)
#define set_gc_vblock(bank, vblock)   (g_misc_meta[bank].gc_vblock = vblock)
#define set_lpn(bank, page_num, lpn)  (g_misc_meta[bank].lpn_list_of_cur_vblock[page_num] = lpn)
#define get_lpn(bank, page_num)       (g_misc_meta[bank].lpn_list_of_cur_vblock[page_num])
#define get_miscblk_vpn(bank)         (g_misc_meta[bank].cur_miscblk_vpn)
#define set_miscblk_vpn(bank, vpn)    (g_misc_meta[bank].cur_miscblk_vpn = vpn)
#define get_mapblk_vpn(bank, mapblk_lbn)      (g_misc_meta[bank].cur_mapblk_vpn[mapblk_lbn])
#define set_mapblk_vpn(bank, mapblk_lbn, vpn) (g_misc_meta[bank].cur_mapblk_vpn[mapblk_lbn] = vpn)
#define CHECK_LPAGE(lpn)              ASSERT((lpn) < NUM_LPAGES)
#define CHECK_VPAGE(vpn)              ASSERT((vpn) < (VBLKS_PER_BANK * PAGES_PER_BLK))

//----------------------------------
// FTL internal function prototype
//----------------------------------
static void   format(void);
static void   write_format_mark(void);
static void   sanity_check(void);
static void   load_pmap_table(void);
static void   load_misc_metadata(void);
static void   init_metadata_sram(void);
static void   load_metadata(void);
static void   logging_pmap_table(void);
static void   logging_misc_metadata(void);
static void   write_page(UINT32 const lpn, UINT32 const sect_offset, UINT32 const num_sectors, UINT32 const addr);
static void   ftl_write_page(UINT32 const lpn, UINT32 const sect_offset, UINT32 const num_sectors );
static void   set_vpn(UINT32 const lpn, UINT32 const vpn);
static void   garbage_collection(UINT32 const bank);
static void   set_vcount(UINT32 const bank, UINT32 const vblock, UINT32 const vcount);
static BOOL32 is_bad_block(UINT32 const bank, UINT32 const vblock);
static BOOL32 check_format_mark(void);
static UINT32 get_vcount(UINT32 const bank, UINT32 const vblock);
static UINT32 get_vpn(UINT32 const lpn);
static UINT32 get_vt_vblock(UINT32 const bank);
static UINT32 assign_new_write_vpn(UINT32 const bank);

static void sanity_check(void)
{
    UINT32 dram_requirement = RD_BUF_BYTES + WR_BUF_BYTES + COPY_BUF_BYTES + FTL_BUF_BYTES
        + HIL_BUF_BYTES + TEMP_BUF_BYTES + BAD_BLK_BMP_BYTES + PAGE_MAP_BYTES + VCOUNT_BYTES + LFM_BYTES + LFM_PAGE_BYTES + PAD_BYTES;

    if ((dram_requirement > DRAM_SIZE) || // DRAM metadata size check
        (sizeof(misc_metadata) > BYTES_PER_PAGE)) // misc metadata size check
    {
        led_blink();
        while (1);
    }
}
static void build_bad_blk_list(void)
{
	UINT32 bank, num_entries, result, vblk_offset;
	scan_list_t* scan_list = (scan_list_t*) TEMP_BUF_ADDR;

	mem_set_dram(BAD_BLK_BMP_ADDR, NULL, BAD_BLK_BMP_BYTES);

	disable_irq();

	flash_clear_irq();

	for (bank = 0; bank < NUM_BANKS; bank++)
	{
		SETREG(FCP_CMD, FC_COL_ROW_READ_OUT);
		SETREG(FCP_BANK, REAL_BANK(bank));
		SETREG(FCP_OPTION, FO_E);
		SETREG(FCP_DMA_ADDR, (UINT32) scan_list);
		SETREG(FCP_DMA_CNT, SCAN_LIST_SIZE);
		SETREG(FCP_COL, 0);
		SETREG(FCP_ROW_L(bank), SCAN_LIST_PAGE_OFFSET);
		SETREG(FCP_ROW_H(bank), SCAN_LIST_PAGE_OFFSET);

		SETREG(FCP_ISSUE, NULL);
		while ((GETREG(WR_STAT) & 0x00000001) != 0);
		while (BSP_FSM(bank) != BANK_IDLE);

		num_entries = NULL;
		result = OK;

		if (BSP_INTR(bank) & FIRQ_DATA_CORRUPT)
		{
			result = FAIL;
		}
		else
		{
			UINT32 i;

			num_entries = read_dram_16(&(scan_list->num_entries));

			if (num_entries > SCAN_LIST_ITEMS)
			{
				result = FAIL;
			}
			else
			{
				for (i = 0; i < num_entries; i++)
				{
					UINT16 entry = read_dram_16(scan_list->list + i);
					UINT16 pblk_offset = entry & 0x7FFF;

					if (pblk_offset == 0 || pblk_offset >= PBLKS_PER_BANK)
					{
						#if OPTION_REDUCED_CAPACITY == FALSE
						result = FAIL;
						#endif
					}
					else
					{
						write_dram_16(scan_list->list + i, pblk_offset);
					}
				}
			}
		}

		if (result == FAIL)
		{
			num_entries = 0;  // We cannot trust this scan list. Perhaps a software bug.
		}
		else
		{
			write_dram_16(&(scan_list->num_entries), 0);
		}

		g_bad_blk_count[bank] = 0;

		for (vblk_offset = 1; vblk_offset < VBLKS_PER_BANK; vblk_offset++)
		{
			BOOL32 bad = FALSE;

			#if OPTION_2_PLANE
			{
				UINT32 pblk_offset;

				pblk_offset = vblk_offset * NUM_PLANES;

                // fix bug@jasmine v.1.1.0
				if (mem_search_equ_dram(scan_list, sizeof(UINT16), num_entries + 1, pblk_offset) < num_entries + 1)
				{
					bad = TRUE;
				}

				pblk_offset = vblk_offset * NUM_PLANES + 1;

                // fix bug@jasmine v.1.1.0
				if (mem_search_equ_dram(scan_list, sizeof(UINT16), num_entries + 1, pblk_offset) < num_entries + 1)
				{
					bad = TRUE;
				}
			}
			#else
			{
                // fix bug@jasmine v.1.1.0
				if (mem_search_equ_dram(scan_list, sizeof(UINT16), num_entries + 1, vblk_offset) < num_entries + 1)
				{
					bad = TRUE;
				}
			}
#endif

			if (bad)
			{
				g_bad_blk_count[bank]++;
				set_bit_dram(BAD_BLK_BMP_ADDR + bank*(VBLKS_PER_BANK/8 + 1), vblk_offset);
			}
		}
	}
}

void ftl_open(void)
{
	// debugging example 1 - use breakpoint statement!
	/* *(UINT32*)0xFFFFFFFE = 10; */

	/* UINT32 volatile g_break = 0; */
	/* while (g_break == 0); */

	led(0);
	sanity_check();
	//----------------------------------------
	// read scan lists from NAND flash
	// and build bitmap of bad blocks
	//----------------------------------------
	build_bad_blk_list();

	//----------------------------------------
	// If necessary, do low-level format
	// format() should be called after loading scan lists, because format() calls is_bad_block().
	//----------------------------------------
	/* 	if (check_format_mark() == FALSE) */
#if NO_FORMAT
	if(check_format_mark() == FALSE)
#else
	if (TRUE)
#endif
	{
		uart_print("do format");
		format();
		uart_print("end format");
	}
	// load FTL metadata
	else
	{
		load_metadata();
	}
	g_ftl_read_buf_id = 0;
	g_ftl_write_buf_id = 0;
	int i = 0 ;
	_cmd = 0;
	for(i = 0 ;i < MAX_NUM_FILE; i++)
	{
		cur_pos[MAX_NUM_FILE] = 0;
		cur_bytes[MAX_NUM_FILE] = 0;
	}
	
	// This example FTL can handle runtime bad block interrupts and read fail (uncorrectable bit errors) interrupts
	flash_clear_irq();

	SETREG(INTR_MASK, FIRQ_DATA_CORRUPT | FIRQ_BADBLK_L | FIRQ_BADBLK_H);
	SETREG(FCONF_PAUSE, FIRQ_DATA_CORRUPT | FIRQ_BADBLK_L | FIRQ_BADBLK_H);

	enable_irq();
}
void ftl_flush(void)
{
	/* ptimer_start(); */
	logging_sub_pmap_table();
	logging_pmap_table();
	logging_misc_metadata();
	/* ptimer_stop_and_uart_print(); */
}

void ftl_read(UINT32 const lba, UINT32 const num_sectors)
{
    UINT32 remain_sects, num_sectors_to_read;
    UINT32 lpn, sect_offset;
    UINT32 bank, vpn;

    lpn          = lba / SECTORS_PER_PAGE;
    sect_offset  = lba % SECTORS_PER_PAGE;
    remain_sects = num_sectors;

    while (remain_sects != 0)
    {
        if ((sect_offset + remain_sects) < SECTORS_PER_PAGE)
        {
            num_sectors_to_read = remain_sects;
        }
        else
        {
            num_sectors_to_read = SECTORS_PER_PAGE - sect_offset;
        }
        bank = get_num_bank(lpn); // page striping
        vpn  = get_vpn(lpn);
        CHECK_VPAGE(vpn);

        if (vpn != NULL)
        {
            nand_page_ptread_to_host(bank,
                                     vpn / PAGES_PER_BLK,
                                     vpn % PAGES_PER_BLK,
                                     sect_offset,
                                     num_sectors_to_read);
        }
        // The host is requesting to read a logical page that has never been written to.
        else
        {
			UINT32 next_read_buf_id = (g_ftl_read_buf_id + 1) % NUM_RD_BUFFERS;

			#if OPTION_FTL_TEST == 0
			while (next_read_buf_id == GETREG(SATA_RBUF_PTR));	// wait if the read buffer is full (slow host)
			#endif

            // fix bug @ v.1.0.6
            // Send 0xFF...FF to host when the host request to read the sector that has never been written.
            // In old version, for example, if the host request to read unwritten sector 0 after programming in sector 1, Jasmine would send 0x00...00 to host.
            // However, if the host already wrote to sector 1, Jasmine would send 0xFF...FF to host when host request to read sector 0. (ftl_read() in ftl_xxx/ftl.c)
			mem_set_dram(RD_BUF_PTR(g_ftl_read_buf_id) + sect_offset*BYTES_PER_SECTOR,
                         0xFFFFFFFF, num_sectors_to_read*BYTES_PER_SECTOR);

            flash_finish();

			SETREG(BM_STACK_RDSET, next_read_buf_id);	// change bm_read_limit
			SETREG(BM_STACK_RESET, 0x02);				// change bm_read_limit

			g_ftl_read_buf_id = next_read_buf_id;
        }
        sect_offset   = 0;
        remain_sects -= num_sectors_to_read;
        lpn++;
    }
}
void ftl_write(UINT32 const lba, UINT32 const num_sectors)
{
    UINT32 remain_sects, num_sectors_to_write;
    UINT32 lpn, sect_offset;

    lpn          = lba / SECTORS_PER_PAGE;
    sect_offset  = lba % SECTORS_PER_PAGE;
    remain_sects = num_sectors;

    while (remain_sects != 0)
    {
        if ((sect_offset + remain_sects) < SECTORS_PER_PAGE)
        {
            num_sectors_to_write = remain_sects;
        }
        else
        {
            num_sectors_to_write = SECTORS_PER_PAGE - sect_offset;
        }
        // single page write individually
        ftl_write_page(lpn, sect_offset, num_sectors_to_write);

        sect_offset   = 0;
        remain_sects -= num_sectors_to_write;
        lpn++;
    }
}
static void ftl_write_page(UINT32 const lpn, UINT32 const sect_offset, UINT32 const num_sectors)
{
    CHECK_LPAGE(lpn);
    ASSERT(sect_offset < SECTORS_PER_PAGE);
    ASSERT(num_sectors > 0 && num_sectors <= SECTORS_PER_PAGE);

    UINT32 bank, old_vpn, new_vpn;
    UINT32 vblock, page_num, page_offset, column_cnt;

    bank        = get_num_bank(lpn); // page striping
    page_offset = sect_offset;
    column_cnt  = num_sectors;

    new_vpn  = assign_new_write_vpn(bank);
    old_vpn  = get_vpn(lpn);

    CHECK_VPAGE (old_vpn);
    CHECK_VPAGE (new_vpn);
    ASSERT(old_vpn != new_vpn);

    g_ftl_statistics[bank].page_wcount++;

    // if old data already exist,
    if (old_vpn != NULL)
    {
        vblock   = old_vpn / PAGES_PER_BLK;
        page_num = old_vpn % PAGES_PER_BLK;

        //--------------------------------------------------------------------------------------
        // `Partial programming'
        // we could not determine whether the new data is loaded in the SATA write buffer.
        // Thus, read the left/right hole sectors of a valid page and copy into the write buffer.
        // And then, program whole valid data
        //--------------------------------------------------------------------------------------
        if (num_sectors != SECTORS_PER_PAGE)
        {
            // Performance optimization (but, not proved)
            // To reduce flash memory access, valid hole copy into SATA write buffer after reading whole page
            // Thus, in this case, we need just one full page read + one or two mem_copy
            if ((num_sectors <= 8) && (page_offset != 0))
            {
                // one page async read
                nand_page_read(bank,
                               vblock,
                               page_num,
                               FTL_BUF(bank));
                // copy `left hole sectors' into SATA write buffer
                if (page_offset != 0)
                {
                    mem_copy(WR_BUF_PTR(g_ftl_write_buf_id),
                             FTL_BUF(bank),
                             page_offset * BYTES_PER_SECTOR);
                }
                // copy `right hole sectors' into SATA write buffer
                if ((page_offset + column_cnt) < SECTORS_PER_PAGE)
                {
                    UINT32 const rhole_base = (page_offset + column_cnt) * BYTES_PER_SECTOR;

                    mem_copy(WR_BUF_PTR(g_ftl_write_buf_id) + rhole_base,
                             FTL_BUF(bank) + rhole_base,
                             BYTES_PER_PAGE - rhole_base);
                }
            }
            // left/right hole async read operation (two partial page read)
            else
            {
                // read `left hole sectors'
                if (page_offset != 0)
                {
                    nand_page_ptread(bank,
                                     vblock,
                                     page_num,
                                     0,
                                     page_offset,
                                     WR_BUF_PTR(g_ftl_write_buf_id),
                                     RETURN_ON_ISSUE);
                }
                // read `right hole sectors'
                if ((page_offset + column_cnt) < SECTORS_PER_PAGE)
                {
                    nand_page_ptread(bank,
                                     vblock,
                                     page_num,
                                     page_offset + column_cnt,
                                     SECTORS_PER_PAGE - (page_offset + column_cnt),
                                     WR_BUF_PTR(g_ftl_write_buf_id),
                                     RETURN_ON_ISSUE);
                }
            }
        }
        // full page write
        page_offset = 0;
        column_cnt  = SECTORS_PER_PAGE;
        // invalid old page (decrease vcount)
        set_vcount(bank, vblock, get_vcount(bank, vblock) - 1);
    }
    vblock   = new_vpn / PAGES_PER_BLK;
    page_num = new_vpn % PAGES_PER_BLK;
    ASSERT(get_vcount(bank,vblock) < (PAGES_PER_BLK - 1));

    // write new data (make sure that the new data is ready in the write buffer frame)
    // (c.f FO_B_SATA_W flag in flash.h)
    nand_page_ptprogram_from_host(bank,
                                  vblock,
                                  page_num,
                                  page_offset,
                                  column_cnt);
    // update metadata
    set_lpn(bank, page_num, lpn);
    set_vpn(lpn, new_vpn);
    set_vcount(bank, vblock, get_vcount(bank, vblock) + 1);
}

void lfm_read(UINT32 const lba, UINT32 const num_sector, UINT32 const addr)
{
	UINT32 remain_sects, num_sectors_to_read;
	UINT32 lpn, sect_offset;
	UINT32 bank, vpn;

	UINT32 raddr = addr; 
	lpn          = lba / SECTORS_PER_PAGE;
	sect_offset  = lba % SECTORS_PER_PAGE;
	remain_sects = num_sector;

	while (remain_sects != 0)
	{
		if ((sect_offset + remain_sects) < SECTORS_PER_PAGE)
		{
			num_sectors_to_read = remain_sects;
		}
		else
		{
			num_sectors_to_read = SECTORS_PER_PAGE - sect_offset;
		}
		bank = get_num_bank(lpn); // page striping
		vpn  = get_vpn(lpn);
		CHECK_VPAGE(vpn);

		if (vpn != NULL)
		{
			nand_page_ptread(bank,
					vpn / PAGES_PER_BLK,
					vpn % PAGES_PER_BLK,
					sect_offset,
					num_sectors_to_read,
					raddr - (sect_offset * BYTES_PER_SECTOR),
					//addr , 
					RETURN_WHEN_DONE);
		}
		// The host is requesting to read a logical page that has never been written to.
		sect_offset   = 0;
		remain_sects -= num_sectors_to_read;
		raddr += ( num_sectors_to_read * BYTES_PER_SECTOR );
		lpn++;
	}
}
void lfm_write(UINT32 const lba, UINT32 const num_sectors, UINT32 const addr)
{
	UINT32 remain_sects, num_sectors_to_write;
	UINT32 lpn, sect_offset;

	lpn          = lba / SECTORS_PER_PAGE;
	sect_offset  = lba % SECTORS_PER_PAGE;
	remain_sects = num_sectors;

	while (remain_sects != 0)
	{
		if ((sect_offset + remain_sects) < SECTORS_PER_PAGE)
		{
			num_sectors_to_write = remain_sects;
		}
		else
		{
			num_sectors_to_write = SECTORS_PER_PAGE - sect_offset;
		}
		// single page write individually
		write_page(lpn, sect_offset, num_sectors_to_write, addr - (sect_offset * BYTES_PER_SECTOR));

		sect_offset   = 0;
		remain_sects -= num_sectors_to_write;
		lpn++;
	}
}
static void write_page(UINT32 const lpn, UINT32 const sect_offset, UINT32 const num_sectors, UINT32 const addr)
{
	CHECK_LPAGE(lpn);
	ASSERT(sect_offset < SECTORS_PER_PAGE);
	ASSERT(num_sectors > 0 && num_sectors <= SECTORS_PER_PAGE);

	UINT32 bank, old_vpn, new_vpn;
	UINT32 vblock, page_num, page_offset, column_cnt;

	bank        = get_num_bank(lpn); // page striping
	page_offset = sect_offset;
	column_cnt  = num_sectors;

	new_vpn  = assign_new_write_vpn(bank);
	old_vpn  = get_vpn(lpn);

	CHECK_VPAGE (old_vpn);
	CHECK_VPAGE (new_vpn);
	ASSERT(old_vpn != new_vpn);

	g_ftl_statistics[bank].page_wcount++;

	// if old data already exist,
	if (old_vpn != NULL)
	{
		vblock   = old_vpn / PAGES_PER_BLK;
		page_num = old_vpn % PAGES_PER_BLK;

		//--------------------------------------------------------------------------------------
		// `Partial programming'
		// we could not determine whether the new data is loaded in the SATA write buffer.
		// Thus, read the left/right hole sectors of a valid page and copy into the write buffer.
		// And then, program whole valid data
		//--------------------------------------------------------------------------------------
		if (num_sectors != SECTORS_PER_PAGE)
		{
			// Performance optimization (but, not proved)
			// To reduce flash memory access, valid hole copy into SATA write buffer after reading whole page
			// Thus, in this case, we need just one full page read + one or two mem_copy
			if ((num_sectors <= 8) && (page_offset != 0))
			{
				// one page async read
				nand_page_read(bank,
						vblock,
						page_num,
						FTL_BUF(bank));
				// copy `left hole sectors' into SATA write buffer
				if (page_offset != 0)
				{
					mem_copy( addr,
							FTL_BUF(bank),
							page_offset * BYTES_PER_SECTOR);
				}
				// copy `right hole sectors' into SATA write buffer
				if ((page_offset + column_cnt) < SECTORS_PER_PAGE)
				{
					UINT32 const rhole_base = (page_offset + column_cnt) * BYTES_PER_SECTOR;

					mem_copy(addr + rhole_base,
							FTL_BUF(bank) + rhole_base,
							BYTES_PER_PAGE - rhole_base);
				}
			}
			// left/right hole async read operation (two partial page read)
			else
			{
				// read `left hole sectors'
				if (page_offset != 0)
				{
					nand_page_ptread(bank,
							vblock,
							page_num,
							0,
							page_offset,
							addr,
							RETURN_ON_ISSUE);
				}
				// read `right hole sectors'
				if ((page_offset + column_cnt) < SECTORS_PER_PAGE)
				{
					nand_page_ptread(bank,
							vblock,
							page_num,
							page_offset + column_cnt,
							SECTORS_PER_PAGE - (page_offset + column_cnt),
							addr,
							RETURN_ON_ISSUE);
				}
			}
		}
		// full page write
		page_offset = 0;
		column_cnt  = SECTORS_PER_PAGE;
		// invalid old page (decrease vcount)
		set_vcount(bank, vblock, get_vcount(bank, vblock) - 1);
	}
	vblock   = new_vpn / PAGES_PER_BLK;
	page_num = new_vpn % PAGES_PER_BLK;
	ASSERT(get_vcount(bank,vblock) < (PAGES_PER_BLK - 1));

	// write new data (make sure that the new data is ready in the write buffer frame)
	// (c.f FO_B_SATA_W flag in flash.h)
	nand_page_ptprogram(bank,
			vblock,
			page_num,
			page_offset,
			column_cnt,
			addr );
	// update metadata
	set_lpn(bank, page_num, lpn);
	set_vpn(lpn, new_vpn);
	set_vcount(bank, vblock, get_vcount(bank, vblock) + 1);
}
// get vpn from PAGE_MAP
static UINT32 get_vpn(UINT32 const lpn)
{
	CHECK_LPAGE(lpn);
	return read_dram_32(PAGE_MAP_ADDR + lpn * sizeof(UINT32));
}
// set vpn to PAGE_MAP
static void set_vpn(UINT32 const lpn, UINT32 const vpn)
{
	CHECK_LPAGE(lpn);
	ASSERT(vpn >= (META_BLKS_PER_BANK * PAGES_PER_BLK) && vpn < (VBLKS_PER_BANK * PAGES_PER_BLK));

	write_dram_32(PAGE_MAP_ADDR + lpn * sizeof(UINT32), vpn);
}
// get valid page count of vblock
static UINT32 get_vcount(UINT32 const bank, UINT32 const vblock)
{
	UINT32 vcount;

	ASSERT(bank < NUM_BANKS);
	ASSERT((vblock >= META_BLKS_PER_BANK) && (vblock < VBLKS_PER_BANK));

	vcount = read_dram_16(VCOUNT_ADDR + (((bank * VBLKS_PER_BANK) + vblock) * sizeof(UINT16)));
	ASSERT((vcount < PAGES_PER_BLK) || (vcount == VC_MAX));

	return vcount;
}
// set valid page count of vblock
static void set_vcount(UINT32 const bank, UINT32 const vblock, UINT32 const vcount)
{
	ASSERT(bank < NUM_BANKS);
	ASSERT((vblock >= META_BLKS_PER_BANK) && (vblock < VBLKS_PER_BANK));
	ASSERT((vcount < PAGES_PER_BLK) || (vcount == VC_MAX));

	write_dram_16(VCOUNT_ADDR + (((bank * VBLKS_PER_BANK) + vblock) * sizeof(UINT16)), vcount);
}
static UINT32 assign_new_write_vpn(UINT32 const bank)
{
	ASSERT(bank < NUM_BANKS);

	UINT32 write_vpn;
	UINT32 vblock;

	write_vpn = get_cur_write_vpn(bank);
	vblock    = write_vpn / PAGES_PER_BLK;

	// NOTE: if next new write page's offset is
	// the last page offset of vblock (i.e. PAGES_PER_BLK - 1),
	if ((write_vpn % PAGES_PER_BLK) == (PAGES_PER_BLK - 2))
	{
		// then, because of the flash controller limitation
		// (prohibit accessing a spare area (i.e. OOB)),
		// thus, we persistenly write a lpn list into last page of vblock.
		mem_copy(FTL_BUF(bank), g_misc_meta[bank].lpn_list_of_cur_vblock, sizeof(UINT32) * PAGES_PER_BLK);
		// fix minor bug
		nand_page_ptprogram(bank, vblock, PAGES_PER_BLK - 1, 0,
				((sizeof(UINT32) * PAGES_PER_BLK + BYTES_PER_SECTOR - 1 ) / BYTES_PER_SECTOR), FTL_BUF(bank));

		mem_set_sram(g_misc_meta[bank].lpn_list_of_cur_vblock, 0x00000000, sizeof(UINT32) * PAGES_PER_BLK);

		inc_full_blk_cnt(bank);

		// do garbage collection if necessary
		if (is_full_all_blks(bank))
		{
			garbage_collection(bank);
			//lfm_garbage_collection();
			return get_cur_write_vpn(bank);
		}
		do
		{
			vblock++;

			ASSERT(vblock != VBLKS_PER_BANK);
		}while (get_vcount(bank, vblock) == VC_MAX);
	}
	// write page -> next block
	if (vblock != (write_vpn / PAGES_PER_BLK))
	{
		write_vpn = vblock * PAGES_PER_BLK;
	}
	else
	{
		write_vpn++;
	}
	set_new_write_vpn(bank, write_vpn);

	return write_vpn;
}
static BOOL32 is_bad_block(UINT32 const bank, UINT32 const vblk_offset)
{
	if (tst_bit_dram(BAD_BLK_BMP_ADDR + bank*(VBLKS_PER_BANK/8 + 1), vblk_offset) == FALSE)
	{
		return FALSE;
	}
	return TRUE;
}
//------------------------------------------------------------
// if all blocks except one free block are full,
// do garbage collection for making at least one free page
//-------------------------------------------------------------
static void garbage_collection(UINT32 const bank)
{
	ASSERT(bank < NUM_BANKS);
	g_ftl_statistics[bank].gc_cnt++;

	UINT32 src_lpn;
	UINT32 vt_vblock;
	UINT32 free_vpn;
	UINT32 vcount; // valid page count in victim block
	UINT32 src_page;
	UINT32 gc_vblock;

	g_ftl_statistics[bank].gc_cnt++;

	vt_vblock = get_vt_vblock(bank);   // get victim block
	vcount    = get_vcount(bank, vt_vblock);
	gc_vblock = get_gc_vblock(bank);
	free_vpn  = gc_vblock * PAGES_PER_BLK;

	/*     uart_printf("garbage_collection bank %d, vblock %d",bank, vt_vblock); */

	ASSERT(vt_vblock != gc_vblock);
	ASSERT(vt_vblock >= META_BLKS_PER_BANK && vt_vblock < VBLKS_PER_BANK);
	ASSERT(vcount < (PAGES_PER_BLK - 1));
	ASSERT(get_vcount(bank, gc_vblock) == VC_MAX);
	ASSERT(!is_bad_block(bank, gc_vblock));

	// 1. load p2l list from last page offset of victim block (4B x PAGES_PER_BLK)
	// fix minor bug
	nand_page_ptread(bank, vt_vblock, PAGES_PER_BLK - 1, 0,
			((sizeof(UINT32) * PAGES_PER_BLK + BYTES_PER_SECTOR - 1 ) / BYTES_PER_SECTOR), FTL_BUF(bank), RETURN_WHEN_DONE);
	mem_copy(g_misc_meta[bank].lpn_list_of_cur_vblock, FTL_BUF(bank), sizeof(UINT32) * PAGES_PER_BLK);
	// 2. copy-back all valid pages to free space
	for (src_page = 0; src_page < (PAGES_PER_BLK - 1); src_page++)
	{
		// get lpn of victim block from a read lpn list
		src_lpn = get_lpn(bank, src_page);
		CHECK_VPAGE(get_vpn(src_lpn));

		// determine whether the page is valid or not
		if (get_vpn(src_lpn) !=
				((vt_vblock * PAGES_PER_BLK) + src_page))
		{
			// invalid page
			continue;
		}
		ASSERT(get_lpn(bank, src_page) != INVALID);
		CHECK_LPAGE(src_lpn);
		// if the page is valid,
		// then do copy-back op. to free space
		nand_page_copyback(bank,
				vt_vblock,
				src_page,
				free_vpn / PAGES_PER_BLK,
				free_vpn % PAGES_PER_BLK);
		ASSERT((free_vpn / PAGES_PER_BLK) == gc_vblock);
		// update metadata
		set_vpn(src_lpn, free_vpn);
		set_lpn(bank, (free_vpn % PAGES_PER_BLK), src_lpn);

		free_vpn++;
	}
#if OPTION_ENABLE_ASSERT
	if (vcount == 0)
	{
		ASSERT(free_vpn == (gc_vblock * PAGES_PER_BLK));
	}
#endif
	// 3. erase victim block
	nand_block_erase(bank, vt_vblock);
	ASSERT((free_vpn % PAGES_PER_BLK) < (PAGES_PER_BLK - 2));
	ASSERT((free_vpn % PAGES_PER_BLK == vcount));

	/*     uart_printf("gc page count : %d", vcount); */

	// 4. update metadata
	set_vcount(bank, vt_vblock, VC_MAX);
	set_vcount(bank, gc_vblock, vcount);
	set_new_write_vpn(bank, free_vpn); // set a free page for new write
	set_gc_vblock(bank, vt_vblock); // next free block (reserve for GC)
	dec_full_blk_cnt(bank); // decrease full block count
	/* uart_print("garbage_collection end"); */
}
//-------------------------------------------------------------
// Victim selection policy: Greedy
//
// Select the block which contain minumum valid pages
//-------------------------------------------------------------
static UINT32 get_vt_vblock(UINT32 const bank)
{
	ASSERT(bank < NUM_BANKS);

	UINT32 vblock;

	// search the block which has mininum valid pages
	vblock = mem_search_min_max(VCOUNT_ADDR + (bank * VBLKS_PER_BANK * sizeof(UINT16)),
			sizeof(UINT16),
			VBLKS_PER_BANK,
			MU_CMD_SEARCH_MIN_DRAM);

	ASSERT(is_bad_block(bank, vblock) == FALSE);
	ASSERT(vblock >= META_BLKS_PER_BANK && vblock < VBLKS_PER_BANK);
	ASSERT(get_vcount(bank, vblock) < (PAGES_PER_BLK - 1));

	return vblock;
}
static void format(void)
{
	UINT32 bank, vblock, vcount_val;

	ASSERT(NUM_MISC_META_SECT > 0);
	ASSERT(NUM_VCOUNT_SECT > 0);

	uart_printf("Total FTL DRAM metadata size: %d KB", DRAM_BYTES_OTHER / 1024);

	uart_printf("VBLKS_PER_BANK: %d", VBLKS_PER_BANK);
	uart_printf("LBLKS_PER_BANK: %d", NUM_LPAGES / PAGES_PER_BLK / NUM_BANKS);
	uart_printf("META_BLKS_PER_BANK: %d", META_BLKS_PER_BANK);

	//----------------------------------------
	// initialize DRAM metadata
	//----------------------------------------
	mem_set_dram(PAGE_MAP_ADDR, NULL, PAGE_MAP_BYTES);
	mem_set_dram(VCOUNT_ADDR, NULL, VCOUNT_BYTES);

	// lfm : ogh FIXME
	mem_set_dram(LFM_ADDR, NULL, LFM_BYTES);

	int i;
	for( i = 0 ;i < MAX_NUM_FILE; i++) 
		write_dram_8(LFM_ADDR + i * ENTRY_BYTES, -1);
	

	//----------------------------------------
	// erase all blocks except vblock #0
	//----------------------------------------
	for (vblock = MISCBLK_VBN; vblock < VBLKS_PER_BANK; vblock++)
	{
		for (bank = 0; bank < NUM_BANKS; bank++)
		{
			vcount_val = VC_MAX;
			if (is_bad_block(bank, vblock) == FALSE)
			{
				nand_block_erase(bank, vblock);
				vcount_val = 0;
			}
			write_dram_16(VCOUNT_ADDR + ((bank * VBLKS_PER_BANK) + vblock) * sizeof(UINT16),
					vcount_val);
		}
	}
	//----------------------------------------
	// initialize SRAM metadata
	//----------------------------------------
	init_metadata_sram();

	// flush metadata to NAND
	logging_sub_pmap_table();
	logging_pmap_table();
	logging_misc_metadata();

	write_format_mark();
	led(1);
	uart_print("format complete");
}
static void init_metadata_sram(void)
{
	UINT32 bank;
	UINT32 vblock;
	UINT32 mapblk_lbn;

	//----------------------------------------
	// initialize misc. metadata
	//----------------------------------------
	// lfm : ogh FIxME
	/* JLEE
	g_misc_meta[0].start_pos = SECTORS_PER_PAGE * 128 ; 
	g_misc_meta[0].end_pos = SECTORS_PER_PAGE * 128 ;
	*/
	int i;
	g_misc_meta[0].start_pos = LFM_INIT_POS; // JLEE
	g_misc_meta[0].end_pos = LFM_INIT_POS - SECTORS_PER_PAGE; // JLEE
	g_misc_meta[0].total_size = 0 ; // JLEE
	g_misc_meta[0].num_entry = 0 ;
	for(i=0; i<MAX_NUM_FILE; i++)
		create_file(i, "");

	for (bank = 0; bank < NUM_BANKS; bank++)
	{
		g_misc_meta[bank].free_blk_cnt = VBLKS_PER_BANK - META_BLKS_PER_BANK;
		g_misc_meta[bank].free_blk_cnt -= get_bad_blk_cnt(bank);
		// NOTE: vblock #0,1 don't use for user space
		write_dram_16(VCOUNT_ADDR + ((bank * VBLKS_PER_BANK) + 0) * sizeof(UINT16), VC_MAX);
		write_dram_16(VCOUNT_ADDR + ((bank * VBLKS_PER_BANK) + 1) * sizeof(UINT16), VC_MAX);

		//----------------------------------------
		// assign misc. block
		//----------------------------------------
		// assumption: vblock #1 = fixed location.
		// Thus if vblock #1 is a bad block, it should be allocate another block.
		set_miscblk_vpn(bank, MISCBLK_VBN * PAGES_PER_BLK - 1);
		ASSERT(is_bad_block(bank, MISCBLK_VBN) == FALSE);

		vblock = MISCBLK_VBN;


		// lfm : ogh FIXME

		write_dram_16(VCOUNT_ADDR + ((bank * VBLKS_PER_BANK) + 2) * sizeof(UINT16), VC_MAX);
		set_logblk_vpn(bank, LOGBLK_VBN * PAGES_PER_BLK -1 );
		vblock++;
		// lfm end ]] 

		//----------------------------------------
		// assign map block
		//----------------------------------------
		mapblk_lbn = 0;
		while (mapblk_lbn < MAPBLKS_PER_BANK)
		{
			vblock++;
			ASSERT(vblock < VBLKS_PER_BANK);
			if (is_bad_block(bank, vblock) == FALSE)
			{
				set_mapblk_vpn(bank, mapblk_lbn, vblock * PAGES_PER_BLK);
				write_dram_16(VCOUNT_ADDR + ((bank * VBLKS_PER_BANK) + vblock) * sizeof(UINT16), VC_MAX);
				mapblk_lbn++;
			}
		}
		//----------------------------------------
		// assign free block for gc
		//----------------------------------------
		do
		{
			vblock++;
			// NOTE: free block should not be secleted as a victim @ first GC
			write_dram_16(VCOUNT_ADDR + ((bank * VBLKS_PER_BANK) + vblock) * sizeof(UINT16), VC_MAX);
			// set free block
			set_gc_vblock(bank, vblock);

			ASSERT(vblock < VBLKS_PER_BANK);
		}while(is_bad_block(bank, vblock) == TRUE);
		//----------------------------------------
		// assign free vpn for first new write
		//----------------------------------------
		do
		{
			vblock++;
			// 현재 next vblock부터 새로운 데이터를 저장을 시작
			set_new_write_vpn(bank, vblock * PAGES_PER_BLK);
			ASSERT(vblock < VBLKS_PER_BANK);
		}while(is_bad_block(bank, vblock) == TRUE);
	}
}
// logging misc + vcount metadata
static void logging_misc_metadata(void)
{
	UINT32 misc_meta_bytes = NUM_MISC_META_SECT * BYTES_PER_SECTOR; // per bank
	UINT32 vcount_addr     = VCOUNT_ADDR;
	UINT32 vcount_bytes    = NUM_VCOUNT_SECT * BYTES_PER_SECTOR; // per bank
	UINT32 vcount_boundary = VCOUNT_ADDR + VCOUNT_BYTES; // entire vcount data
	UINT32 bank;

	flash_finish();

	for (bank = 0; bank < NUM_BANKS; bank++)
	{
		inc_miscblk_vpn(bank);

		// note: if misc. meta block is full, just erase old block & write offset #0
		if ((get_miscblk_vpn(bank) / PAGES_PER_BLK) != MISCBLK_VBN)
		{
			nand_block_erase(bank, MISCBLK_VBN);
			set_miscblk_vpn(bank, MISCBLK_VBN * PAGES_PER_BLK); // vpn = 128
		}
		// copy misc. metadata to FTL buffer
		mem_copy(FTL_BUF(bank), &g_misc_meta[bank], misc_meta_bytes);

		// copy vcount metadata to FTL buffer
		if (vcount_addr <= vcount_boundary)
		{
			mem_copy(FTL_BUF(bank) + misc_meta_bytes, vcount_addr, vcount_bytes);
			vcount_addr += vcount_bytes;
		}
	}
	// logging the misc. metadata to nand flash
	for (bank = 0; bank < NUM_BANKS; bank++)
	{
		nand_page_ptprogram(bank,
				get_miscblk_vpn(bank) / PAGES_PER_BLK,
				get_miscblk_vpn(bank) % PAGES_PER_BLK,
				0,
				NUM_MISC_META_SECT + NUM_VCOUNT_SECT,
				FTL_BUF(bank));
	}
	flash_finish();
}
static void logging_pmap_table(void)
{
	UINT32 pmap_addr  = PAGE_MAP_ADDR;
	UINT32 pmap_bytes = BYTES_PER_PAGE; // per bank
	UINT32 mapblk_vpn;
	UINT32 bank;
	UINT32 pmap_boundary = PAGE_MAP_ADDR + PAGE_MAP_BYTES;
	BOOL32 finished = FALSE;

	for (UINT32 mapblk_lbn = 0; mapblk_lbn < MAPBLKS_PER_BANK; mapblk_lbn++)
	{
		flash_finish();

		for (bank = 0; bank < NUM_BANKS; bank++)
		{
			if (finished)
			{
				break;
			}
			else if (pmap_addr >= pmap_boundary)
			{
				finished = TRUE;
				break;
			}
			else if (pmap_addr + BYTES_PER_PAGE >= pmap_boundary)
			{
				finished = TRUE;
				pmap_bytes = (pmap_boundary - pmap_addr + BYTES_PER_SECTOR - 1) / BYTES_PER_SECTOR * BYTES_PER_SECTOR ;
			}
			inc_mapblk_vpn(bank, mapblk_lbn);

			mapblk_vpn = get_mapblk_vpn(bank, mapblk_lbn);

			// note: if there is no free page, then erase old map block first.
			if ((mapblk_vpn % PAGES_PER_BLK) == 0)
			{
				// erase full map block
				nand_block_erase(bank, (mapblk_vpn - 1) / PAGES_PER_BLK);

				// next vpn of mapblk is offset #0
				set_mapblk_vpn(bank, mapblk_lbn, ((mapblk_vpn - 1) / PAGES_PER_BLK) * PAGES_PER_BLK);
				mapblk_vpn = get_mapblk_vpn(bank, mapblk_lbn);
			}
			// copy the page mapping table to FTL buffer
			mem_copy(FTL_BUF(bank), pmap_addr, pmap_bytes);

			// logging update page mapping table into map_block
			nand_page_ptprogram(bank,
					mapblk_vpn / PAGES_PER_BLK,
					mapblk_vpn % PAGES_PER_BLK,
					0,
					pmap_bytes / BYTES_PER_SECTOR,
					FTL_BUF(bank));
			pmap_addr += pmap_bytes;
		}
		if (finished)
		{
			break;
		}
	}
	flash_finish();
}

// load flushed FTL metadta
static void load_metadata(void)
{
	load_misc_metadata();
	load_pmap_table();
	load_sub_pmap_table();
}
// misc + VCOUNT
static void load_misc_metadata(void)
{
	UINT32 misc_meta_bytes = NUM_MISC_META_SECT * BYTES_PER_SECTOR;
	UINT32 vcount_bytes    = NUM_VCOUNT_SECT * BYTES_PER_SECTOR;
	UINT32 vcount_addr     = VCOUNT_ADDR;
	UINT32 vcount_boundary = VCOUNT_ADDR + VCOUNT_BYTES;

	UINT32 load_flag = 0;
	UINT32 bank, page_num;
	UINT32 load_cnt = 0;

	flash_finish();

	disable_irq();
	flash_clear_irq();	// clear any flash interrupt flags that might have been set

	// scan valid metadata in descending order from last page offset
	for (page_num = PAGES_PER_BLK - 1; page_num != ((UINT32) -1); page_num--)
	{
		for (bank = 0; bank < NUM_BANKS; bank++)
		{
			if (load_flag & (0x1 << bank))
			{
				continue;
			}
			// read valid metadata from misc. metadata area
			nand_page_ptread(bank,
					MISCBLK_VBN,
					page_num,
					0,
					NUM_MISC_META_SECT + NUM_VCOUNT_SECT,
					FTL_BUF(bank),
					RETURN_ON_ISSUE);
		}
		flash_finish();

		for (bank = 0; bank < NUM_BANKS; bank++)
		{
			if (!(load_flag & (0x1 << bank)) && !(BSP_INTR(bank) & FIRQ_ALL_FF))
			{
				load_flag = load_flag | (0x1 << bank);
				load_cnt++;
			}
			CLR_BSP_INTR(bank, 0xFF);
		}
	}
	ASSERT(load_cnt == NUM_BANKS);

	for (bank = 0; bank < NUM_BANKS; bank++)
	{
		// misc. metadata
		mem_copy(&g_misc_meta[bank], FTL_BUF(bank), sizeof(misc_metadata));

		// vcount metadata
		if (vcount_addr <= vcount_boundary)
		{
			mem_copy(vcount_addr, FTL_BUF(bank) + misc_meta_bytes, vcount_bytes);
			vcount_addr += vcount_bytes;

		}
	}
	enable_irq();
}
static void load_pmap_table(void)
{
	UINT32 pmap_addr = PAGE_MAP_ADDR;
	UINT32 temp_page_addr;
	UINT32 pmap_bytes = BYTES_PER_PAGE; // per bank
	UINT32 pmap_boundary = PAGE_MAP_ADDR + (NUM_LPAGES * sizeof(UINT32));
	UINT32 mapblk_lbn, bank;
	BOOL32 finished = FALSE;

	flash_finish();

	for (mapblk_lbn = 0; mapblk_lbn < MAPBLKS_PER_BANK; mapblk_lbn++)
	{
		temp_page_addr = pmap_addr; // backup page mapping addr

		for (bank = 0; bank < NUM_BANKS; bank++)
		{
			if (finished)
			{
				break;
			}
			else if (pmap_addr >= pmap_boundary)
			{
				finished = TRUE;
				break;
			}
			else if (pmap_addr + BYTES_PER_PAGE >= pmap_boundary)
			{
				finished = TRUE;
				pmap_bytes = (pmap_boundary - pmap_addr + BYTES_PER_SECTOR - 1) / BYTES_PER_SECTOR * BYTES_PER_SECTOR;
			}
			// read page mapping table from map_block
			nand_page_ptread(bank,
					get_mapblk_vpn(bank, mapblk_lbn) / PAGES_PER_BLK,
					get_mapblk_vpn(bank, mapblk_lbn) % PAGES_PER_BLK,
					0,
					pmap_bytes / BYTES_PER_SECTOR,
					FTL_BUF(bank),
					RETURN_ON_ISSUE);
			pmap_addr += pmap_bytes;
		}
		flash_finish();

		pmap_bytes = BYTES_PER_PAGE;
		for (bank = 0; bank < NUM_BANKS; bank++)
		{
			if (temp_page_addr >= pmap_boundary)
			{
				break;
			}
			else if (temp_page_addr + BYTES_PER_PAGE >= pmap_boundary)
			{
				pmap_bytes = (pmap_boundary - temp_page_addr + BYTES_PER_SECTOR - 1) / BYTES_PER_SECTOR * BYTES_PER_SECTOR;
			}
			// copy page mapping table to PMAP_ADDR from FTL buffer
			mem_copy(temp_page_addr, FTL_BUF(bank), pmap_bytes);

			temp_page_addr += pmap_bytes;
		}
		if (finished)
		{
			break;
		}
	}
}
static void write_format_mark(void)
{
	// This function writes a format mark to a page at (bank #0, block #0).

#ifdef __GNUC__
	extern UINT32 size_of_firmware_image;
	UINT32 firmware_image_pages = (((UINT32) (&size_of_firmware_image)) + BYTES_PER_FW_PAGE - 1) / BYTES_PER_FW_PAGE;
#else
	extern UINT32 Image$$ER_CODE$$RO$$Length;
	extern UINT32 Image$$ER_RW$$RW$$Length;
	UINT32 firmware_image_bytes = ((UINT32) &Image$$ER_CODE$$RO$$Length) + ((UINT32) &Image$$ER_RW$$RW$$Length);
	UINT32 firmware_image_pages = (firmware_image_bytes + BYTES_PER_FW_PAGE - 1) / BYTES_PER_FW_PAGE;
#endif

	UINT32 format_mark_page_offset = FW_PAGE_OFFSET + firmware_image_pages;

	mem_set_dram(FTL_BUF_ADDR, 0, BYTES_PER_SECTOR);

	SETREG(FCP_CMD, FC_COL_ROW_IN_PROG);
	SETREG(FCP_BANK, REAL_BANK(0));
	SETREG(FCP_OPTION, FO_E | FO_B_W_DRDY);
	SETREG(FCP_DMA_ADDR, FTL_BUF_ADDR); 	// DRAM -> flash
	SETREG(FCP_DMA_CNT, BYTES_PER_SECTOR);
	SETREG(FCP_COL, 0);
	SETREG(FCP_ROW_L(0), format_mark_page_offset);
	SETREG(FCP_ROW_H(0), format_mark_page_offset);

	// At this point, we do not have to check Waiting Room status before issuing a command,
	// because we have waited for all the banks to become idle before returning from format().
	SETREG(FCP_ISSUE, NULL);

	// wait for the FC_COL_ROW_IN_PROG command to be accepted by bank #0
	while ((GETREG(WR_STAT) & 0x00000001) != 0);

	// wait until bank #0 finishes the write operation
	while (BSP_FSM(0) != BANK_IDLE);
}
static BOOL32 check_format_mark(void)
{
	// This function reads a flash page from (bank #0, block #0) in order to check whether the SSD is formatted or not.

#ifdef __GNUC__
	extern UINT32 size_of_firmware_image;
	UINT32 firmware_image_pages = (((UINT32) (&size_of_firmware_image)) + BYTES_PER_FW_PAGE - 1) / BYTES_PER_FW_PAGE;
#else
	extern UINT32 Image$$ER_CODE$$RO$$Length;
	extern UINT32 Image$$ER_RW$$RW$$Length;
	UINT32 firmware_image_bytes = ((UINT32) &Image$$ER_CODE$$RO$$Length) + ((UINT32) &Image$$ER_RW$$RW$$Length);
	UINT32 firmware_image_pages = (firmware_image_bytes + BYTES_PER_FW_PAGE - 1) / BYTES_PER_FW_PAGE;
#endif

	UINT32 format_mark_page_offset = FW_PAGE_OFFSET + firmware_image_pages;
	UINT32 temp;

	flash_clear_irq();	// clear any flash interrupt flags that might have been set

	SETREG(FCP_CMD, FC_COL_ROW_READ_OUT);
	SETREG(FCP_BANK, REAL_BANK(0));
	SETREG(FCP_OPTION, FO_E);
	SETREG(FCP_DMA_ADDR, FTL_BUF_ADDR); 	// flash -> DRAM
	SETREG(FCP_DMA_CNT, BYTES_PER_SECTOR);
	SETREG(FCP_COL, 0);
	SETREG(FCP_ROW_L(0), format_mark_page_offset);
	SETREG(FCP_ROW_H(0), format_mark_page_offset);

	// At this point, we do not have to check Waiting Room status before issuing a command,
	// because scan list loading has been completed just before this function is called.
	SETREG(FCP_ISSUE, NULL);

	// wait for the FC_COL_ROW_READ_OUT command to be accepted by bank #0
	while ((GETREG(WR_STAT) & 0x00000001) != 0);

	// wait until bank #0 finishes the read operation
	while (BSP_FSM(0) != BANK_IDLE);

	// Now that the read operation is complete, we can check interrupt flags.
	temp = BSP_INTR(0) & FIRQ_ALL_FF;

	// clear interrupt flags
	CLR_BSP_INTR(0, 0xFF);

	if (temp != 0)
	{
		return FALSE;	// the page contains all-0xFF (the format mark does not exist.)
	}
	else
	{
		return TRUE;	// the page contains something other than 0xFF (it must be the format mark)
	}
}

// BSP interrupt service routine
void ftl_isr(void)
{
	UINT32 bank;
	UINT32 bsp_intr_flag;

	uart_print("BSP interrupt occured...");
	// interrupt pending clear (ICU)
	SETREG(APB_INT_STS, INTR_FLASH);

	for (bank = 0; bank < NUM_BANKS; bank++) {
		while (BSP_FSM(bank) != BANK_IDLE);
		// get interrupt flag from BSP
		bsp_intr_flag = BSP_INTR(bank);

		if (bsp_intr_flag == 0) {
			continue;
		}
		UINT32 fc = GETREG(BSP_CMD(bank));
		// BSP clear
		CLR_BSP_INTR(bank, bsp_intr_flag);

		// interrupt handling
		if (bsp_intr_flag & FIRQ_DATA_CORRUPT) {
			uart_printf("BSP interrupt at bank: 0x%x", bank);
			uart_print("FIRQ_DATA_CORRUPT occured...");
		}
		if (bsp_intr_flag & (FIRQ_BADBLK_H | FIRQ_BADBLK_L)) {
			uart_printf("BSP interrupt at bank: 0x%x", bank);
			if (fc == FC_COL_ROW_IN_PROG || fc == FC_IN_PROG || fc == FC_PROG) {
				uart_print("find runtime bad block when block program...");
			}
			else {
				uart_printf("find runtime bad block when block erase...vblock #: %d", GETREG(BSP_ROW_H(bank)) / PAGES_PER_BLK);
				ASSERT(fc == FC_ERASE);
			}
		}
	}
}

// lfm layer : ogh FIXME
/* 
 * Usage of memory utility 
 return read_dram_32(PAGE_MAP_ADDR + lpn * sizeof(UINT32));
 write_dram_32(PAGE_MAP_ADDR + lpn * sizeof(UINT32), vpn);
 */


typedef struct lfm_struct
{
	UINT32 id;
	UINT32 st_pos;
	char name[MAX_FILE_NAME];

	UINT32 end_pos;	// JLEE
	UINT32 cur_pos;	// JLEE
	UINT32 cur_bytes;	// JLEE
	UINT32 prev_size;	// JLEE
	UINT32 total_size;	// JLEE
	UINT32 actual_size;	// JLEE
	UINT32 mark_pos;	// JLEE
}lfm_desc;

#define LFM_ADDR_ID(idx)		(LFM_ADDR + (idx)*ENTRY_BYTES)
#define LFM_ADDR_ST_POS(idx)		(LFM_ADDR + (idx)*ENTRY_BYTES + sizeof(UINT32))
#define LFM_ADDR_NAME(idx)		(LFM_ADDR + (idx)*ENTRY_BYTES + sizeof(UINT32)*2)
#define LFM_ADDR_END_POS(idx)		(LFM_ADDR + (idx)*ENTRY_BYTES + sizeof(UINT32)*2 + sizeof(char)*MAX_FILE_NAME)
#define LFM_ADDR_CUR_POS(idx)		(LFM_ADDR + (idx)*ENTRY_BYTES + sizeof(UINT32)*3 + sizeof(char)*MAX_FILE_NAME)
#define LFM_ADDR_CUR_BYTES(idx)		(LFM_ADDR + (idx)*ENTRY_BYTES + sizeof(UINT32)*4 + sizeof(char)*MAX_FILE_NAME)
#define LFM_ADDR_PREV_SIZE(idx)		(LFM_ADDR + (idx)*ENTRY_BYTES + sizeof(UINT32)*5 + sizeof(char)*MAX_FILE_NAME)
#define LFM_ADDR_TOTAL_SIZE(idx)	(LFM_ADDR + (idx)*ENTRY_BYTES + sizeof(UINT32)*6 + sizeof(char)*MAX_FILE_NAME)
#define LFM_ADDR_ACTUAL_SIZE(idx)	(LFM_ADDR + (idx)*ENTRY_BYTES + sizeof(UINT32)*7 + sizeof(char)*MAX_FILE_NAME)
#define LFM_ADDR_MARK_POS(idx)		(LFM_ADDR + (idx)*ENTRY_BYTES + sizeof(UINT32)*8 + sizeof(char)*MAX_FILE_NAME)

typedef struct _ack
{
	UINT8 cmd, id;
	UINT16 size;
	UINT32 error;
}ack;
static void lfm_garbage_collection(void)
{
	UINT32 lsn = g_misc_meta[0].start_pos;
	UINT32 bank = get_num_bank(lsn);
	UINT32 sect_offset = lsn % SECTORS_PER_PAGE;
	UINT32 blk = (lsn % (SECTORS_PER_BANK)) / (SECTORS_PER_PAGE * PAGES_PER_BLK);
	UINT32 addr = LFM_PAGE_ADDR + BYTES_PER_PAGE;
	UINT32 n_addr = addr;
	UINT32 size, id, pointer;
	UINT32 c_lsn;
	UINT32 idx ;
	UINT32 num_sectors = SECTORS_PER_PAGE - sect_offset;
	UINT32 remain_sectors = 0;
	UINT32 size_sectors; 
	int i;

	for(i = 0 ;i < PAGES_PER_BLK ;i++)
	{
		// one page read 
		num_sectors = SECTORS_PER_PAGE - sect_offset;
		lfm_read(lsn, num_sectors, addr);
		n_addr = addr;
		c_lsn = lsn;
		if( remain_sectors != 0 )
		{
			pointer = read_dram_32(n_addr + BYTES_PER_SECTOR * remain_sectors - sizeof(UINT32));
		}
		while(num_sectors > 0)
		{
			if( remain_sectors == 0 )
			{
				// get log data size
				size = read_dram_32(n_addr);
				n_addr += sizeof(UINT32);

				//get file id 
				id = read_dram_32(n_addr);
				idx = get_file_idx(id);

				n_addr += sizeof(UINT32);

				size_sectors = ((align_to_dram(size) + sizeof(UINT32) * 3 + BYTES_PER_SECTOR -1 ) / BYTES_PER_SECTOR);
				if( size_sectors > num_sectors )
				{
					break;
				}
				//get next data pointer 
				pointer = read_dram_32( n_addr + align_to_dram(size) );
				num_sectors -= size_sectors;
			}
			else
			{
				num_sectors -= remain_sectors;
			}
			remain_sectors = 0;
			if( idx != MAX_NUM_FILE )
			{
				// if next pointer is set
				// then that is latest data of that lsn
				if( pointer != c_lsn  )
				{
					// change start position 
					write_dram_32(LFM_ADDR + idx * ENTRY_BYTES + sizeof(UINT32), pointer);
					// reduce total size
					total_size[idx] -= size;
					g_misc_meta[0].total_size -= size;
				}
				// if lsn == end position ,
				// it is end of file. 
				else if( c_lsn == end_pos[idx] )
				{
					write_dram_32(LFM_ADDR + idx * ENTRY_BYTES + sizeof(UINT32), 0);
					end_pos[idx] = 0;
					total_size[idx] = 0;
					g_misc_meta[0].total_size -= size;
				}
			}
			n_addr += (sizeof(UINT32) * 3  + align_to_dram(size));
		}
		remain_sectors = size_sectors - num_sectors;
		lsn += (SECTORS_PER_PAGE - sect_offset);
		sect_offset = 0 ;
	}
	// change global start position 
	g_misc_meta[0].start_pos += SECTORS_PER_BANK;

	if( remain_sectors != 0 )
	{
		num_sectors = SECTORS_PER_PAGE;
		lfm_read(lsn, num_sectors, addr);
		n_addr = addr;
		c_lsn = lsn;
		pointer = read_dram_32(n_addr + BYTES_PER_SECTOR * remain_sectors - sizeof(UINT32));
		// change start position 
		g_misc_meta[0].start_pos += remain_sectors; 
		if( idx != MAX_NUM_FILE )
		{
			// if next pointer is set
			// then that is latest data of that lsn
			if( pointer != c_lsn  )
			{
				// change start position 
				write_dram_32(LFM_ADDR + idx * ENTRY_BYTES + sizeof(UINT32), pointer);
				// reduce total size
				total_size[idx] -= size;
				g_misc_meta[0].total_size -= size;
			}
			// if lsn == end position ,
			// it is end of file. 
			else if( c_lsn == end_pos[idx] )
			{
				write_dram_32(LFM_ADDR + idx * ENTRY_BYTES + sizeof(UINT32), 0);
				end_pos[idx] = 0;
				total_size[idx] = 0;
				g_misc_meta[0].total_size -= size;
			}
		}
	}
	nand_block_erase( bank, blk);
	set_new_write_vpn( bank, blk * PAGES_PER_BLK );
}

typedef struct _aligned_ack
{
	ack _ack;
	UINT8 pad[DRAM_ECC_UNIT - sizeof(ack)];
}aligned_ack;
static void send_ack(UINT8 id, UINT16 size, UINT32 error)
{
	aligned_ack val;

	val._ack.cmd = 0x10;
	val._ack.id = id;
	val._ack.size = size;
	val._ack.error = error;
	write_to_sata( (char*)&val, sizeof(ack) );
}

// copy from ftl_read
static void write_to_sata(char *addr, UINT32 size)
{
	int i;
	for(i =0 ;i < _sectors;i++)
	{
		UINT32 next_read_buf_id = (g_ftl_read_buf_id + 1) % NUM_RD_BUFFERS;

#if OPTION_FTL_TEST == 0
		while (next_read_buf_id == GETREG(SATA_RBUF_PTR));	// wait if the read buffer is full (slow host)
#endif

		copy_data(RD_BUF_PTR(g_ftl_read_buf_id),
				(UINT32)addr,
				size );
		flash_finish();
		SETREG(BM_STACK_RDSET, next_read_buf_id);
		SETREG(BM_STACK_RESET, 0x02);

		g_ftl_read_buf_id = next_read_buf_id;
	}
}
// copy from ftl_write
static void read_sata_buffer(UINT32 idx)
{
	mem_copy(LFM_PAGE_ADDR + idx * BYTES_PER_PAGE,
			WR_BUF_PTR(g_ftl_write_buf_id),
			BYTES_PER_PAGE);
	g_ftl_write_buf_id = (g_ftl_write_buf_id + 1) % NUM_WR_BUFFERS;	
}

void init_lfm(void)
{
}

// add new entry 
// parsing argument page before this function 
int create_file(UINT32 id, char* name)
{
	if(g_misc_meta[0].num_entry >= MAX_NUM_FILE)
		return MAX_NUM_FILE;

	UINT32 num_entry = g_misc_meta[0].num_entry++;

	/* set new entry */
	lfm_desc new;

	mem_set_sram(&new,0,sizeof(lfm_desc));
	new.id = id;
	mem_copy(new.name, name, MAX_FILE_NAME );

	end_pos[num_entry] = 0;
	cur_pos[num_entry] = 0;
	cur_bytes[num_entry] = 0;
	prev_size[num_entry] = 0;
	total_size[num_entry] = 0 ;
	actual_size[num_entry] = 0 ;
	mark_pos[num_entry] = 0 ;
	// copy new entry data to file desc table 
	mem_copy(LFM_ADDR + num_entry * ENTRY_BYTES, &new, sizeof(lfm_desc));

	// for recovery
	logging_sub_pmap_table();
	logging_misc_metadata();

	return num_entry;
}


void open_file(UINT8 id, char *name)
{
	if(id < MAX_NUM_FILE) {
		cur_pos[id] = read_dram_32( LFM_ADDR + id * ENTRY_BYTES + sizeof(UINT32) );
		cur_bytes[id] = 0 ; 
		if(name[0]!='\0') {
			uart_printf("Name %d changed to %s", id, name);	// JLEE_DEBUG
			mem_copy(LFM_ADDR_NAME(id), name, MAX_FILE_NAME);
		}
		send_ack(id,8,0);
	} else {
		send_ack(id,8,4);
	}
/*
	int i;
	int n_entry = g_misc_meta[0].num_entry;

	UINT32 addr;
	for(i = 0 ;i < n_entry ; i++)
	{
		addr = LFM_ADDR + i * ENTRY_BYTES;
		// if file exists already 
		if(id == read_dram_32(addr) )
		{
			// read start pos from table
			cur_pos[i] = read_dram_32( addr + sizeof(UINT32) );
			cur_bytes[i] = 0 ; 
			if(name[0]!='\0') {
				uart_printf("Name %d changed to %s", id, name);	// JLEE_DEBUG
				mem_copy(LFM_ADDR_NAME(i), name, MAX_FILE_NAME);
			}
			// reset current pos 
			send_ack(id, 8, 0);
			return;
		}
	}
	// if file does not exist, 
	// then create new file and add it to table
	if(create_file(id,name) < MAX_NUM_FILE)
		send_ack(id,8,0);
	else
		send_ack(id,8,4);
*/
}


void read_file(UINT8 id , UINT32 req_size, UINT8 flag)
{
	int i ;
	UINT32 pos;
	UINT32 bytes;
	UINT32 size;
	UINT32 num_sectors;
	UINT32 addr = LFM_PAGE_ADDR ;
	int remain_size;
	UINT32 resize; 
	UINT32 ac_size; 	// JLEE
	i = get_file_idx(id);
	if( i == MAX_NUM_FILE )
	{
		send_ack(id,0,2);
		_flag = 0 ;
		return ; 
		/* error, no file */
	}
	else if( flag )
	{

		/* 
		   request size could be bigger than
		   one log file data size 
		   */
		aligned_ack val; // JLEE
		UINT32 read_addr = LFM_PAGE_ADDR + BYTES_PER_PAGE * 2 + sizeof(ack); // JLEE
		ret_size = 0;
			if( req_size > BYTES_PER_PAGE-BYTES_PER_SECTOR)	// JLEE
				req_size = BYTES_PER_PAGE-BYTES_PER_SECTOR;	// JLEE
		while( req_size > 0 )
		{
			if( total_size[i] <= 0 )
				break;

			pos = cur_pos[i];// logical sector number of current position 
			bytes = cur_bytes[i]; // bytes offset of current position 
			lfm_read(pos, SECTORS_PER_PAGE, addr); // read current log data
			// get size of log data
			//size = read_dram_32(addr);
			ac_size = read_dram_32(addr);
			size = BYTES_PER_PAGE - BYTES_PER_SECTOR;
			uart_printf("READ %08x", pos);	// JLEE_DEBUG

			// get # for sectors of log file 
			num_sectors = (align_to_dram(size) + sizeof(UINT32)*3 + BYTES_PER_SECTOR -1 ) / BYTES_PER_SECTOR;
			// remain bytes of log data from current bytes offset 
			remain_size = size - bytes;
			// next entry position 
			pos = read_dram_32(addr + align_to_dram(size) + sizeof(UINT32)*2 );


			if(remain_size == 0 )
			{
				if( cur_pos[i] == pos ) 
				{
					break;
				}
				cur_bytes[i] = 0 ; 
				cur_pos[i] = pos; 
				continue;
			}

			// 0 < request size < remain bytes
			if( remain_size >= req_size )
			{
				resize = req_size; 
			}

			else
			{
				resize = remain_size; 
			}

			resize = (resize+3)/4*4; // JLEE

			copy_data(read_addr, addr + sizeof(UINT32)*2 + bytes, resize );
			// align to 4bytes
			/* JLEE
			read_addr += ((resize+3)/4*4); 

			cur_bytes[i] += resize; 
			req_size -= resize;
			ret_size += ((resize+3)/4*4);
			*/
			read_addr += resize;
			cur_bytes[i] += resize; 
			//ret_size += resize;
			ret_size += ac_size;
			if(size - cur_bytes[i]==0) {
				if( cur_pos[i] == pos ) 
				{
					break;
				}
				cur_bytes[i] = 0 ; 
				cur_pos[i] = pos; 
			}
			if(req_size > resize)
				req_size -= resize;
			else	break;
		}

		// JLEE
		val._ack.cmd = 0x10;
		val._ack.id = id;
		val._ack.size = ret_size;
		val._ack.error = 0;
		//copy_data(LFM_PAGE_ADDR + BYTES_PER_PAGE * 2 + BYTES_PER_PAGE/2, (UINT32)(&val), sizeof(aligned_ack));
		//copy_data(LFM_PAGE_ADDR + BYTES_PER_PAGE * 2, (UINT32)(&val), sizeof(aligned_ack));
		copy_data(LFM_PAGE_ADDR + BYTES_PER_PAGE * 2, (UINT32)(&val), sizeof(ack));

		/* at first send ack message only */
		//send_ack(id, ret_size, 0x00);
		//write_to_sata((char*)(LFM_PAGE_ADDR + BYTES_PER_PAGE * 2), ret_size);
		write_to_sata((char*)(LFM_PAGE_ADDR + BYTES_PER_PAGE * 2), BYTES_PER_PAGE); // JLEE
		/* JLEE
		if( ret_size == 0 ) 
		{
			_flag = 0 ;
		}
		*/
	}
	else
	{
		/* next, send real data to read */
		write_to_sata((char*)(LFM_PAGE_ADDR + BYTES_PER_PAGE * 2),
				ret_size);
	}
	//ftl_flush(); // JLEE
}
static void copy_data (UINT32 dest, UINT32 src, UINT32 size )
{
	/* first, copy Data to DRAM using mem_copy */
	/* it should be aligned to DRAM_ECC_UNIT */
	char buff[1024];
	int rsize = size;
	UINT32 ssize ; 
	while(1)
	{
		if( rsize <= 0 ) break;
		if ( 1024  > rsize )
		{
			ssize = (rsize + 3 ) /4 * 4; 
		}
		else
		{
			ssize = 1024;
		}
		//mem_set_sram(buff,0,sizeof(buff)); // JLEE
		mem_copy( buff, src, ssize );
		mem_copy( dest, buff, ssize);

		src += ssize;
		dest += ssize;
		rsize -= ssize; 

	}

}

/* struct of log file 

// 1byte : size of log
// 1byte : id of log file
// x bytes : log data 
// 4bytes : page number of next data of same log file
// 4bytes : index of next data in page number 

*/


static UINT8 get_file_idx(UINT8 id)
{
	return id;
}

static int is_marked_region(int pos, int mark, int end)
{
	if(mark < end) {
		if(mark <= pos && pos <= end)
			return 1;
		else 	return 0;
	} else {
		if(pos <= end || mark <= pos)
			return 1;
		else	return 0;
	}
}

void write_data(UINT32 id, UINT32 data,const UINT32 ac_size)
{
	UINT32 addr = LFM_PAGE_ADDR + BYTES_PER_PAGE;
	UINT16 rsize ; 
	UINT32 new_pos;
	UINT32 st_pos;
	UINT8 idx = get_file_idx(id);
	UINT32 size = BYTES_PER_PAGE - BYTES_PER_SECTOR;
	int remain_size = size;
	UINT32 raddr; 
	char buff[1024];

	if( idx == MAX_NUM_FILE )
	{
		if( create_file(id, "") == MAX_NUM_FILE ) {
			send_ack(id,8,4);
			return ;
		}
		idx = get_file_idx(id);
	}

	st_pos = read_dram_32(LFM_ADDR + idx * ENTRY_BYTES + sizeof(UINT32));
	/* setting new log data */
	//mem_set_dram( addr , 0, BYTES_PER_PAGE);
	// first 4bytes  for log size
	write_dram_32(addr, ac_size);

	// second 4bytes for file ID 
	addr += sizeof(UINT32);
	write_dram_32(addr, id);

	// real log data, align data size to use mem_copy utility
	addr += sizeof(UINT32);

	raddr = addr; 
	while(1)
	{
		UINT32 s_size;
		if(remain_size <= 0 ) break;
		if( remain_size > 1024 )
		{
			s_size = 1024;
		}
		else
		{
			s_size = (remain_size + 3 ) / 4 * 4;
		}
		/* JLEE
		mem_set_sram(buff,'\r',sizeof(buff));
		*/
		mem_copy( buff, data, s_size);
		mem_copy( raddr, buff, s_size);
		
		raddr += s_size;
		data += s_size; 
		remain_size -= s_size;
	}

	rsize = align_to_dram(size);
	addr += rsize;
	rsize = (sizeof(UINT32) * 3 + rsize + BYTES_PER_SECTOR -1 ) / BYTES_PER_SECTOR; // JLEE (moved from below)

	// write log data to flash

	/* mark end of list */
	//get new position 
	new_pos = g_misc_meta[0].end_pos;
	while(1) {
		new_pos += SECTORS_PER_PAGE;
		if(new_pos + SECTORS_PER_PAGE > LFM_END_POS) 	// JLEE
			new_pos = LFM_INIT_POS; 	// JLEE

		/* JLEE */
		if(new_pos == g_misc_meta[0].start_pos && g_misc_meta[0].total_size > 0) {
			UINT32 gc_addr = LFM_PAGE_ADDR + BYTES_PER_PAGE*2;
			UINT32 gc_size, gc_ac_size, gc_size_sectors;
			UINT32 gc_idx;
			UINT32 gc_st_pos, gc_next;
			uart_printf("\tGC 0x%08x", g_misc_meta[0].start_pos);	// JLEE_DEBUG

			lfm_read( g_misc_meta[0].start_pos, SECTORS_PER_PAGE, gc_addr );

			gc_ac_size = read_dram_32(gc_addr);
			gc_size = BYTES_PER_PAGE - BYTES_PER_SECTOR;
			gc_size_sectors = (align_to_dram(gc_size) + sizeof(UINT32)*3 + BYTES_PER_SECTOR-1) / BYTES_PER_SECTOR;
			gc_addr += sizeof(UINT32);

			gc_idx = get_file_idx( read_dram_32(gc_addr) );
			gc_addr += sizeof(UINT32);

			if(is_marked_region(g_misc_meta[0].start_pos, mark_pos[gc_idx], end_pos[gc_idx])) {
				uart_printf("\tSKIP 0x%08x", g_misc_meta[0].start_pos);	// JLEE_DEBUG
				g_misc_meta[0].start_pos += gc_size_sectors;
				if(g_misc_meta[0].start_pos + SECTORS_PER_PAGE > LFM_END_POS)
					g_misc_meta[0].start_pos = LFM_INIT_POS;
				continue;
			}

			gc_next = read_dram_32( gc_addr + align_to_dram(gc_size) );

			gc_st_pos = read_dram_32(LFM_ADDR + gc_idx*ENTRY_BYTES + sizeof(UINT32));
			if(cur_pos[gc_idx] == gc_st_pos) {
				cur_pos[gc_idx] = gc_next;
				cur_bytes[gc_idx] = 0;
			}
			write_dram_32(LFM_ADDR + gc_idx*ENTRY_BYTES + sizeof(UINT32), gc_next);
			total_size[gc_idx] -= gc_size_sectors;
			actual_size[gc_idx] -= gc_ac_size;
			g_misc_meta[0].total_size -= gc_size_sectors;
			g_misc_meta[0].start_pos += gc_size_sectors;
			if(g_misc_meta[0].start_pos + SECTORS_PER_PAGE > LFM_END_POS)
				g_misc_meta[0].start_pos = LFM_INIT_POS;
			break;
		} else {
			break;
		}
	}

	// write it down to "next list pointer",
	// self pointing mean " I'm end of list of this file. " 
	write_dram_32(addr, new_pos);
	/* end of marking */


	/* caculate # of SECTORS to write log data to NAND */
	/* end flush log data to flash */
	//rsize = (sizeof(UINT32) * 3 + rsize + BYTES_PER_SECTOR -1 ) / BYTES_PER_SECTOR;

	lfm_write(new_pos, rsize, LFM_PAGE_ADDR + BYTES_PER_PAGE);
	/* end of NAND WRITE */


	if( total_size[idx] == 0)
	{
		write_dram_32(LFM_ADDR + idx * ENTRY_BYTES + sizeof(UINT32), new_pos);
		cur_pos[idx] = new_pos;
		cur_bytes[idx] = 0;
		mark_pos[idx] = new_pos;
	}
	else
	{
		/* make link prev -> new */
		// read prev log data and set link 

		addr = LFM_PAGE_ADDR + BYTES_PER_PAGE * 2;
		//lfm_read(end_pos[idx], prev_size[idx], addr );
		lfm_read(end_pos[idx], SECTORS_PER_PAGE, addr );	// JLEE

		//UINT32 psize = read_dram_32(addr);
		UINT32 psize = BYTES_PER_PAGE - BYTES_PER_SECTOR;
		// make a link
		write_dram_32(addr + sizeof(UINT32) * 2 + align_to_dram(psize) , new_pos);

		// write prev log data again
		//lfm_write(end_pos[idx], prev_size[idx], addr);
		lfm_write(end_pos[idx], SECTORS_PER_PAGE, addr);	// JLEE
		/* end of making link */
	}

	end_pos[idx] = new_pos;

	/* JLEE
	total_size[idx] += size;
	*/
	total_size[idx] += rsize;
	actual_size[idx] += ac_size;
	g_misc_meta[0].total_size += rsize;	// JLEE

	// JLEE
	if(actual_size[idx] + BYTES_PER_PAGE > LFM_RETAIN_SIZE) {
		addr = LFM_PAGE_ADDR + BYTES_PER_PAGE * 2;
		lfm_read(mark_pos[idx], SECTORS_PER_PAGE, addr);
		mark_pos[idx] = read_dram_32(addr + sizeof(UINT32) * 2 + align_to_dram(BYTES_PER_PAGE-BYTES_PER_SECTOR));
	}
	uart_printf("WRITE %08x (mark %08x)", new_pos, mark_pos[idx]);	// JLEE_DEBUG

	g_misc_meta[0].end_pos = end_pos[idx];

	/* set global variables */
	prev_size[idx] = rsize;
	/* end of global variables */

	ftl_flush(); // JLEE
	// ack 
	send_ack(id,8,0);
}
// ls command
void display_list(void)
{
	int i =0;
	char fname[MAX_FILE_NAME + 1];
	uart_print("List of the log files\n");
	for(i =0 ;i < g_misc_meta[0].num_entry;i++)
	{
		mem_copy(fname, 
				LFM_ADDR + i * ENTRY_BYTES + sizeof(UINT32) * 2,
				MAX_FILE_NAME);
		fname[MAX_FILE_NAME] = '\0';
		uart_print(fname);
	}
}
// rm command 
void delete_file(char *fname)
{
	int idx;
	char buff[MAX_FILE_NAME + 1];
	for(idx =0 ; idx < g_misc_meta[0].num_entry ;idx++)
	{
		mem_copy(buff, LFM_ADDR + idx * ENTRY_BYTES + sizeof(UINT32) * 2, MAX_FILE_NAME);
		if(!mem_cmp_sram(buff, 
					fname,
					MAX_FILE_NAME))
		{
			break;
		}
	}
	if ( idx == g_misc_meta[0].num_entry )
	{
		uart_print("No files!\n");
		return;
	}
	write_dram_32(LFM_ADDR + idx * ENTRY_BYTES, 0x7FFFFFFF);
	write_dram_32(LFM_ADDR + idx * ENTRY_BYTES + sizeof(UINT32) * 2,
			0x7FFFFFFF);
	uart_print("done\n");
}

void cat_file(char *fname)
{
	int idx;
	UINT32 st_pos;
	UINT32 pointer;
	UINT32 size; 
	UINT32 addr; 
	char buff[4097];
	for(idx =0 ; idx < g_misc_meta[0].num_entry ;idx++)
	{
		mem_copy(buff, LFM_ADDR +idx * ENTRY_BYTES + sizeof(UINT32) * 2 , MAX_FILE_NAME);
		if(!mem_cmp_dram(fname, 
					buff, 
					MAX_FILE_NAME))
		{
			break;
		}
	}
	if ( idx == g_misc_meta[0].num_entry )
	{
		uart_print("No files!\n");
		return;
	}

	st_pos = read_dram_32(LFM_ADDR + idx * ENTRY_BYTES + sizeof(UINT32) );
	while(1)
	{
		addr = LFM_PAGE_ADDR + BYTES_PER_PAGE * 2; 
		lfm_read(st_pos, SECTORS_PER_PAGE, addr);
		size = read_dram_32( addr);
		pointer = read_dram_32(addr + sizeof(UINT32) *2 + align_to_dram(size));
		while( size > 0 )
		{
			if( size > 4096 )
			{
				mem_copy(buff, addr + sizeof(UINT32) * 2, 4096);
				size -= 4096;
				buff[4096] = '\0';
				uart_print(buff);
			}
			else
			{
				mem_copy(buff, addr + sizeof(UINT32) * 2, size);
				buff[size] = '\0';
				size = 0;
				uart_print(buff);
			}
		}
		st_pos = pointer;
	}
}

void down_file(char *fname)
{
}

// send_info by JLEE
void send_info(UINT32 id)
{
	UINT32 read_addr = LFM_PAGE_ADDR + BYTES_PER_PAGE * 2 + sizeof(ack); // JLEE
	int n_entry = g_misc_meta[0].num_entry;
	aligned_ack val;
	int i;
	uart_printf("INFO");	// JLEE_DEBUG

	for(i =0 ;i < n_entry; i++) {
		write_dram_32(LFM_ADDR_END_POS(i), end_pos[i]);
		write_dram_32(LFM_ADDR_CUR_POS(i), cur_pos[i]);
		write_dram_32(LFM_ADDR_CUR_BYTES(i), cur_bytes[i]);
		write_dram_32(LFM_ADDR_PREV_SIZE(i), prev_size[i]);
		write_dram_32(LFM_ADDR_TOTAL_SIZE(i), total_size[i]);
		write_dram_32(LFM_ADDR_ACTUAL_SIZE(i), actual_size[i]);
		write_dram_32(LFM_ADDR_MARK_POS(i), mark_pos[i]);
	}
	copy_data(read_addr, LFM_ADDR, ENTRY_BYTES * n_entry);

	val._ack.cmd = 0x10;
	val._ack.id = id;
	val._ack.size = ENTRY_BYTES * n_entry + 8;
	val._ack.error = 0;
	copy_data(LFM_PAGE_ADDR + BYTES_PER_PAGE * 2, (UINT32)(&val), sizeof(ack));
	write_to_sata((char*)(LFM_PAGE_ADDR + BYTES_PER_PAGE * 2), BYTES_PER_PAGE);
}

// parsing and execute  lml function
UINT8 beff[1024];
int lfm_parser(UINT32 const lba, UINT32 const sectors)
{
	UINT32 addr = LFM_PAGE_ADDR + 2 * BYTES_PER_PAGE;
	int i; 

	_sectors = (sectors + SECTORS_PER_PAGE -1 ) /SECTORS_PER_PAGE;
	if( lba == 0 )
	{

		mem_set_dram(addr,0,BYTES_PER_PAGE);
		mem_copy(addr, 
				WR_BUF_PTR(g_ftl_write_buf_id),
				BYTES_PER_PAGE);
		_cmd = read_dram_8( addr);

		addr += sizeof(UINT8);
		_id = read_dram_8( addr );

		addr += sizeof(UINT8) ;
		_size = read_dram_16( addr ) - 4;

		addr += sizeof(UINT16);


		// open 
		if( _cmd == 0x00)// || _cmd == 0x7f)
		{
			mem_copy( _name, addr, sizeof(UINT8) * (_size+3)/4*4 );
		}
		// read 
		else if ( _cmd == 0x01 )
		{
			_req_size = read_dram_32(addr);
		}
		// write
		else if ( _cmd == 0x02 )
		{
			/* JLEE
			mem_set_sram(beff,0,sizeof(beff));
			mem_copy(beff, addr, (_size + 3) / 4 * 4 );

			mem_set_dram(LFM_PAGE_ADDR + BYTES_PER_PAGE * 3, 0 ,BYTES_PER_PAGE);
			copy_data(LFM_PAGE_ADDR + BYTES_PER_PAGE * 3 , addr, (_size +3) /4 *4);
			_data = LFM_PAGE_ADDR + BYTES_PER_PAGE * 3;
			*/
			//_data = (char *)addr;
			_data = addr;	// JLEE
		}
		// info
		else if ( _cmd == 0x03 )
		{
			// nothing
		}
		// change limit of sata write buffer 
		for(i =0 ;i < _sectors;i++)
		{
			g_ftl_write_buf_id = (g_ftl_write_buf_id + 1) % NUM_WR_BUFFERS;	
		}
	}
	else if( lba == 1 )
	{
		if( _cmd == 0x00 )
		{
			open_file( _id, _name);
		}
		else if ( _cmd == 0x01 )
		{
			/* JLEE
			if( _flag )
			{
				read_file( _id, _req_size, 0);
				_flag = 0 ;
			}
			else
			{
				_flag = 1;
				read_file( _id, _req_size, 1);
			}
			*/
			read_file( _id, _req_size, 1 );
		}
		else if ( _cmd == 0x02 )
		{
			write_data( _id, _data , _size );
		}
		else if ( _cmd == 0x03 )
		{
			send_info( _id );
		}
		else
		{
			for(i = 0 ;i < _sectors;i++)
			{
				UINT32 next_read_buf_id = (g_ftl_read_buf_id + 1) % NUM_RD_BUFFERS;

#if OPTION_FTL_TEST == 0
				while (next_read_buf_id == GETREG(SATA_RBUF_PTR));	// wait if the read buffer is full (slow host)
#endif

				mem_set_dram(RD_BUF_PTR(g_ftl_read_buf_id) ,
						0xFFFFFFFF, BYTES_PER_PAGE);

				flash_finish();

				SETREG(BM_STACK_RDSET, next_read_buf_id);	// change bm_read_limit
				SETREG(BM_STACK_RESET, 0x02);				// change bm_read_limit

				g_ftl_read_buf_id = next_read_buf_id;
			}
		}
	}

	return 0;
}


void load_sub_pmap_table()
{
	UINT32 bank, page_num;

	flash_finish();

	bank = get_cur_logblk_bank();
	page_num = get_logblk_vpn(bank) % PAGES_PER_BLK;
	nand_page_ptread(bank,
			LOGBLK_VBN,
			page_num,
			0,
			NUM_SUB_PMAP_SECT ,
			LFM_ADDR,
			RETURN_ON_ISSUE);

	int i = 0;
	int n_entry = g_misc_meta[0].num_entry;
	UINT32 id, start_pos;
	UINT32 size, r_size, sector_size;
	UINT32 pointer;
	UINT32 addr = LFM_PAGE_ADDR; 

	/* JLEE
	for(i =0 ;i < n_entry;i++)
	{
		start_pos = read_dram_32(LFM_ADDR + i * ENTRY_BYTES + sizeof(UINT32));
		lfm_read(start_pos, SECTORS_PER_PAGE, LFM_PAGE_ADDR);

		size = read_dram_32(addr);
		r_size = align_to_sector( sizeof(UINT32) * 3 + align_to_dram(size) ) / BYTES_PER_SECTOR; // JLEE
		pointer = read_dram_32(addr + (sizeof(UINT32) * 2 + align_to_dram(size)));
		//total_size[i] += size;
		total_size[i] += r_size;

		while(1)
		{
			if( pointer == start_pos )
			{
				break;
			}
			start_pos = pointer;
			lfm_read(start_pos, SECTORS_PER_PAGE, addr); 

			size = read_dram_32(addr);
			r_size = align_to_sector( sizeof(UINT32) * 3 + align_to_dram(size) ) / BYTES_PER_SECTOR; // JLEE
			pointer = read_dram_32(addr + (sizeof(UINT32) * 2 + align_to_dram(size)));
			//total_size[i] += size;
			total_size[i] += r_size;
		}
		//prev_size[i] = align_to_sector( sizeof(UINT32) * 3 + align_to_dram(size) ) / BYTES_PER_SECTOR;
		prev_size[i] = r_size; // JLEE
		end_pos[i] = start_pos;
		if( start_pos == g_misc_meta[0].end_pos )
		{
			last_size = prev_size[i];
		}
		cur_pos[i] = 0;
		cur_bytes[i] = 0;
	}
	last_size = prev_size[i-1];
	*/
	for(i =0 ;i < n_entry;i++) {
		end_pos[i] = read_dram_32(LFM_ADDR_END_POS(i));
		cur_pos[i] = read_dram_32(LFM_ADDR_CUR_POS(i));
		cur_bytes[i] = read_dram_32(LFM_ADDR_CUR_BYTES(i));
		prev_size[i] = read_dram_32(LFM_ADDR_PREV_SIZE(i));
		total_size[i] = read_dram_32(LFM_ADDR_TOTAL_SIZE(i));
		actual_size[i] = read_dram_32(LFM_ADDR_ACTUAL_SIZE(i));
		mark_pos[i] = read_dram_32(LFM_ADDR_MARK_POS(i));
	}
	flash_finish();
}
void logging_sub_pmap_table()
{
	UINT32 bank;

	flash_finish();

	inc_logblk_bank();
	bank = get_cur_logblk_bank();
	inc_logblk_vpn(bank);

	// note: if misc. meta block is full, just erase old block & write offset #0
	if ((get_logblk_vpn(bank) / PAGES_PER_BLK) != LOGBLK_VBN)
	{
		nand_block_erase(bank, LOGBLK_VBN);
		set_logblk_vpn(bank, LOGBLK_VBN * PAGES_PER_BLK); // vpn = 128
	}

	// JLEE
	{
		int i = 0;
		int n_entry = g_misc_meta[0].num_entry;
		for(i =0 ;i < n_entry;i++) {
			write_dram_32(LFM_ADDR_END_POS(i), end_pos[i]);
			write_dram_32(LFM_ADDR_CUR_POS(i), cur_pos[i]);
			write_dram_32(LFM_ADDR_CUR_BYTES(i), cur_bytes[i]);
			write_dram_32(LFM_ADDR_PREV_SIZE(i), prev_size[i]);
			write_dram_32(LFM_ADDR_TOTAL_SIZE(i), total_size[i]);
			write_dram_32(LFM_ADDR_ACTUAL_SIZE(i), actual_size[i]);
			write_dram_32(LFM_ADDR_MARK_POS(i), mark_pos[i]);
		}
	}

	// logging the misc. metadata to nand flash
	//FIXME
	nand_page_ptprogram(bank,
			get_logblk_vpn(bank) / PAGES_PER_BLK,
			get_logblk_vpn(bank) % PAGES_PER_BLK,
			0,
			NUM_SUB_PMAP_SECT,
			LFM_ADDR);

	flash_finish();
}
