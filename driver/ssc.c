#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/errno.h>
#include <linux/types.h>
#include <linux/proc_fs.h>
#include <linux/fcntl.h>
#include <linux/semaphore.h>
#include <asm/uaccess.h>
#include "conf.h"
#include "ssc.h"

//#define		_SSC_DEBUG_

MODULE_LICENSE("Soteria Security");

int ssc_open(struct inode *inode, struct file *filp);
int ssc_release(struct inode *inode, struct file *filp);
ssize_t ssc_read(struct file *filp, char *buf, size_t count, loff_t *f_pos);
ssize_t ssc_write(struct file *filp, const char *buf, size_t count, loff_t *f_pos);
long ssc_ioctl(struct file *filp, unsigned int ioctl_num, unsigned long ioctl_param);
void ssc_exit(void);
int ssc_init(void);

struct file_operations ssc_fops = {
	read: ssc_read,
	write: ssc_write,
	unlocked_ioctl: ssc_ioctl,
	compat_ioctl: ssc_ioctl,
	open: ssc_open,
	release: ssc_release
};

module_init(ssc_init);
module_exit(ssc_exit);

char *ssc_temp_buffer;
char *ssc_buffer;
char *ssc_buffer2;
char bufcheck[BUFCHECK_LEN];

int ssc_pipe_to_dev;
int ssc_pipe_from_dev;

char *buf_data[MAX_FILE_NUM];
int buf_size[MAX_FILE_NUM];

struct semaphore ssc_mutex;


/****************************************************************************
* File access
****************************************************************************/

struct file* file_open(const char* path, int flags, int rights)
{
	struct file* filp = NULL;
	mm_segment_t oldfs;
	int err = 0;

	oldfs = get_fs();
	set_fs(get_ds());

	filp = filp_open(path, flags, rights);

	set_fs(oldfs);
	if(IS_ERR(filp)) {
		err = PTR_ERR(filp);
		return NULL;
	}

	return filp;
}

void file_close(struct file* filp)
{
	filp_close(filp, NULL);
}

int file_read(struct file* filp, char* data, size_t size, loff_t* pos)
{
	mm_segment_t oldfs;
	int ret;

	oldfs = get_fs();
	set_fs(get_ds());
	ret = vfs_read(filp, data, size, pos);
	set_fs(oldfs);
	return ret;
}

int file_write(struct file* filp, char* data, size_t size, loff_t* pos)
{
	mm_segment_t oldfs;
	int ret;
	oldfs = get_fs();
	set_fs(get_ds());
	ret = vfs_write(filp, data, size, pos);
	vfs_fsync(filp, 0);
	set_fs(oldfs);
	return ret;
}

/****************************************************************************
* SSC device access
****************************************************************************/

int ssc_get_pipe_to_dev(void)
{
	int ret = ssc_pipe_to_dev;
	ssc_pipe_to_dev += SSC_PAGE_SIZE*16;
	if(ssc_pipe_to_dev >= PIPE_FROM_DEV)
		ssc_pipe_to_dev = PIPE_TO_DEV;
	return ret;
}

int ssc_get_pipe_from_dev(void)
{
	int ret = ssc_pipe_from_dev;
	ssc_pipe_from_dev += SSC_PAGE_SIZE*16;
	if(ssc_pipe_from_dev >= PIPE_END)
		ssc_pipe_from_dev = PIPE_FROM_DEV;
	return ret;
}

void ssc_dev_error(int error_code)
{
	if(error_code & 0x1)
		printk("<1>SSC: [ALERT] Potential log flooding attack detected\n");
	if(error_code & 0x2)
		printk("<1>SSC: [ERROR] There is no file with the given minor number\n");
	if(error_code & 0x4)
		printk("<1>SSC: [ERROR] The maximum number of log files is reached");
}

int ssc_dev_wait_ack(struct file* fp_dev, int minor)
{
	char* msg_ack = ssc_buffer2;
	unsigned short* data_size = (unsigned short*)&msg_ack[2];
	int* error_code = (int*)&msg_ack[4];
	loff_t pos = ssc_get_pipe_from_dev();

#ifdef	_SSC_DEBUG_
	printk("---function---dev_wait_ack\n");
#endif

	file_read(fp_dev, msg_ack, 8, &pos);
	if(msg_ack[0] != MSG_ACK) {
		printk("<1>SSC: [ERROR] ACK missing: %x\n", msg_ack[0]);
		return 0;
	}
	if(msg_ack[1] != minor) {
		printk("<1>SSC: [ERROR] ACK to %d was expected but %d received\n", minor, msg_ack[1]);
		return 0;
	}
	ssc_dev_error(*error_code);
	return *data_size;
}

void ssc_dev_open(int minor, const char* filename)
{
	struct file* fp_dev;

	fp_dev = file_open(SSC_DEV_PATH, O_RDWR | O_SYNC , 0);
	if(fp_dev) {
#ifdef	_SSC_DEBUG_
		printk("---function---dev_open\n");
#endif
		int i;
		char* msg_open = ssc_buffer;
		unsigned short* msg_size = (unsigned short*)&msg_open[2];
		loff_t pos = ssc_get_pipe_to_dev();

		msg_open[0] = MSG_CREATE;
		msg_open[1] = minor;
		*msg_size = 4;
		i = 0;
		while(filename[i] != '\0') {
			msg_open[i+4] = filename[i];
			(*msg_size)++;
			i++;
			if(i>=MAX_FILE_NAME-1) break;
		}
		msg_open[i+4] = '\0';
		(*msg_size)++;
		while(i<MAX_FILE_NAME) {
			msg_open[i+4] = '\0';
			i++;
		}
		file_write(fp_dev, msg_open, SSC_PAGE_SIZE, &pos);
		ssc_dev_wait_ack(fp_dev, minor);
		file_close(fp_dev);

		for(i=0; i<BUFCHECK_LEN; i++)
			bufcheck[i] = 0;
	}
#ifdef	_SSC_DEBUG_
	printk("end dev_open -----\n");
#endif
}

int ssc_dev_read(int minor, char* data, size_t size)
{
	struct file* fp_dev;
	int ret = 0;
	fp_dev = file_open(SSC_DEV_PATH, O_RDWR  | O_SYNC , 0);
	if(fp_dev) {
#ifdef	_SSC_DEBUG_
		printk("--------------dev read\n");
#endif
		loff_t pos;
		char* msg_read = ssc_buffer2;
		unsigned short* msg_size = (unsigned short*)&msg_read[2];
		int* req_size = (int*)&msg_read[4];

		char* msg_ack = data;
		unsigned short* ret_size = (unsigned short*)&msg_ack[2];
		int* error_code = (int*)&msg_ack[4];

		// Send READ message
		msg_read[0] = MSG_READ;
		msg_read[1] = minor;
		*msg_size = 8;
		*req_size = size;
		pos = ssc_get_pipe_to_dev();

		file_write(fp_dev, msg_read, SSC_PAGE_SIZE, &pos);

#ifdef	_SSC_DEBUG_
		printk("--------------read wait for ack\n");
#endif

		// Receive ACK message and data
		pos = ssc_get_pipe_from_dev() ;
		file_read(fp_dev, data, SSC_PAGE_SIZE, &pos);

		if(msg_ack[0] != MSG_ACK) {
			printk("<1>SSC: [ERROR] ACK missing: %x\n", msg_ack[0]);
			return 0;
		}
		if(msg_ack[1] != minor) {
			printk("<1>SSC: [ERROR] ACK to %d was expected but %d received\n", minor, msg_ack[1]);
			return 0;
		}
		ssc_dev_error(*error_code);
		ret = *ret_size;

		file_close(fp_dev);

	}
#ifdef	_SSC_DEBUG_
	printk("end of read function (ret=%d)----------\n", ret);
#endif
	return ret;
}

int ssc_dev_write(int minor, char* data, size_t size)
{
	struct file* fp_dev;
	int ret = 0;

#ifdef	_SSC_DEBUG_
	printk("----write function \n");
#endif
	fp_dev = file_open(SSC_DEV_PATH, O_RDWR |O_SYNC, 0);
	if(fp_dev) {
		unsigned short* msg_size = (unsigned short*)&data[2];
		loff_t pos = ssc_get_pipe_to_dev();

		data[0] = MSG_APPEND;
		data[1] = minor;
		*msg_size = size + 4;

		ret = file_write(fp_dev, data, SSC_PAGE_SIZE, &pos);
		ssc_dev_wait_ack(fp_dev, minor);

		file_close(fp_dev);
	}
#ifdef	_SSC_DEBUG_
	printk("end write---------\n");
#endif
	return ret;
}

void ssc_dev_info(int minor, unsigned long ioctl_param)
{
	struct file* fp_dev;

	fp_dev = file_open(SSC_DEV_PATH, O_RDWR | O_SYNC , 0);
	if(fp_dev) {
#ifdef	_SSC_DEBUG_
		printk("---function---dev_info\n");
#endif
		loff_t pos;
		char* msg_info = ssc_buffer2;
		unsigned short* msg_size = (unsigned short*)&msg_info[2];

		char* msg_ack = ssc_buffer;
		//unsigned short* ret_size = (unsigned short*)&msg_ack[2];
		int* error_code = (int*)&msg_ack[4];
		SSC_DEV_INFO_DESC_T* ssc_dev_info_desc = (SSC_DEV_INFO_DESC_T*)&msg_ack[8];
		int i;

		// Send INFO message
		msg_info[0] = MSG_INFO;
		msg_info[1] = minor;
		*msg_size = 4;
		pos = ssc_get_pipe_to_dev();

		file_write(fp_dev, msg_info, SSC_PAGE_SIZE, &pos);

#ifdef	_SSC_DEBUG_
		printk("--------------read wait for ack\n");
#endif

		// Receive ACK message and data
		pos = ssc_get_pipe_from_dev() ;
		file_read(fp_dev, msg_ack, SSC_PAGE_SIZE, &pos);

		if(msg_ack[0] != MSG_ACK) {
			printk("<1>SSC: [ERROR] ACK missing: %x\n", msg_ack[0]);
			return;
		}
		if(msg_ack[1] != minor) {
			printk("<1>SSC: [ERROR] ACK to %d was expected but %d received\n", minor, msg_ack[1]);
			return;
		}
		ssc_dev_error(*error_code);

		for(i=0; i<MAX_FILE_NUM; i++) {
			ssc_dev_info_desc[i].name[MAX_FILE_NAME-1] = '\0';
#ifdef	_SSC_DEBUG_
			printk("%d, %s, %d\n", ssc_dev_info_desc[i].id, ssc_dev_info_desc[i].name, ssc_dev_info_desc[i].actual_size);
#endif
			if(ssc_dev_info_desc[i].id == minor) {
				copy_to_user(((char*)ioctl_param)+sizeof(int)*2, &(ssc_dev_info_desc[i]), sizeof(SSC_DEV_INFO_DESC_T));
				break;
			}
		}

		file_close(fp_dev);
	}
#ifdef	_SSC_DEBUG_
	printk("end dev_info -----\n");
#endif
}

/****************************************************************************
 * Buffer
 ****************************************************************************/

void buf_init(void)
{
	int i;
	for(i=0; i<MAX_FILE_NUM; i++) {
		buf_data[i] = NULL;
		buf_size[i] = 0;
	}
}

int buf_flush(int minor)
{
#ifdef	_SSC_DEBUG_
	printk("buffer flush\n");
#endif
	if(buf_size[minor]>0) {
		size_t padding = SSC_PAGE_SIZE - MARGIN_WDATA - buf_size[minor];

		memcpy(ssc_buffer+OFFSET_WDATA, buf_data[minor], buf_size[minor]);
		memset(ssc_buffer+OFFSET_WDATA+buf_size[minor], 0, padding);
		ssc_dev_write(minor, ssc_buffer, buf_size[minor]);

		kfree(buf_data[minor]);
		buf_data[minor] = NULL;
		buf_size[minor] = 0;
	}

	return 0;
}

ssize_t buf_write(int minor, const char *buf, size_t count)
{
	size_t remain, req_count;
	int offset;
	int is_flush;
#ifdef	_SSC_DEBUG_
	printk("buffer write %d, %d\n", count, buf_size[minor]);
#endif

	remain = count;
	offset = 0;
	while(remain>0) {
		if(remain > SSC_PAGE_SIZE-MARGIN_WDATA-buf_size[minor]) {
			req_count = SSC_PAGE_SIZE-MARGIN_WDATA-buf_size[minor];
			is_flush = 1;
		} else {
			req_count = remain;
			is_flush = 0;
		}

		if(buf_size[minor] == 0) {
			buf_data[minor] = kmalloc(SSC_PAGE_SIZE, GFP_KERNEL);
		}

		copy_from_user(buf_data[minor]+buf_size[minor], buf+offset, req_count);
		buf_size[minor] += req_count;
		offset += req_count;
		remain -= req_count;

		if(is_flush == 1)
			buf_flush(minor);
	}

	return count;
}

void buf_exit(void)
{
	int i;
	for(i=0; i<MAX_FILE_NUM; i++) {
		buf_flush(i);
	}
}

/****************************************************************************
 * Main APIs
 ****************************************************************************/

int ssc_init(void)
{
	int result;

	result = register_chrdev(SSC_MAJOR, "ssc_dev", &ssc_fops);
	if(result<0) {
		printk("<1>SSC: cannot obtain major number %d\n", SSC_MAJOR);
		return result;
	}

	ssc_temp_buffer = kmalloc(SSC_PAGE_SIZE*3, GFP_KERNEL);
	ssc_buffer = (char*)((int)ssc_temp_buffer & (~(SSC_PAGE_SIZE-1)));
	ssc_buffer2 = ssc_buffer + SSC_PAGE_SIZE;
	if(!ssc_temp_buffer) {
		result = -ENOMEM;
		goto fail;
	}

	buf_init();

	ssc_pipe_to_dev = PIPE_TO_DEV;
	ssc_pipe_from_dev = PIPE_FROM_DEV;

	sema_init(&ssc_mutex, 1);

#ifdef	_SSC_DEBUG_
	printk("<1>Inserting SSC module\n");
#endif
	return 0;

fail:
#ifdef	_SSC_DEBUG_
	printk("fail to init\n");
#endif
	ssc_exit();
	return result;
}

void ssc_exit(void)
{
	buf_exit();

	unregister_chrdev(SSC_MAJOR, "ssc_dev");

	if(ssc_temp_buffer)
		kfree(ssc_temp_buffer);

#ifdef	_SSC_DEBUG_
	printk("<1>Removing SSC module\n");
#endif
}

int ssc_open(struct inode *inode, struct file *filp)
{
#ifdef	_SSC_DEBUG_
	printk("<1>Open SSC module %d\n", MINOR(inode->i_rdev));
#endif

	return 0;
}

int ssc_release(struct inode *inode, struct file *filp)
{
#ifdef	_SSC_DEBUG_
	printk("<1>Close SSC module\n");
#endif
	return 0;
}

ssize_t ssc_read(struct file *filp, char *buf, size_t count, loff_t *f_pos)
{
	ssize_t rsize;
	ssize_t ret;
	size_t eff_count;
	int minor;
	int is_dup;
	int i;

	if(down_interruptible(&ssc_mutex))
		return -ERESTARTSYS;

	rsize = 0;
	ret = 0;
	eff_count = ( (count > SSC_PAGE_SIZE-MARGIN_WDATA) ? SSC_PAGE_SIZE-MARGIN_WDATA : (count+3)/4*4 );
	minor = MINOR(filp->f_dentry->d_inode->i_rdev);

	buf_flush(minor);
	rsize = ssc_dev_read(minor, ssc_buffer, eff_count);
	is_dup = rsize;
	for(i=0; i<rsize && i<BUFCHECK_LEN; i++)
		if(bufcheck[i] != ssc_buffer[OFFSET_RDATA + i]) {
			bufcheck[i] = ssc_buffer[OFFSET_RDATA + i];
			is_dup = 0;
		}
	if(is_dup) {
		//printk("duplicated page\n");
		rsize = ssc_dev_read(minor, ssc_buffer, eff_count);
		for(i=0; i<rsize && i<BUFCHECK_LEN; i++)
			bufcheck[i] = ssc_buffer[OFFSET_RDATA + i];
	}

	copy_to_user(buf, ssc_buffer + OFFSET_RDATA, rsize);
	ret = rsize;

	up(&ssc_mutex);

	return ret;
}

ssize_t ssc_write(struct file *filp, const char *buf, size_t count, loff_t *f_pos)
{
	int minor;
	ssize_t ret;

	if(down_interruptible(&ssc_mutex))
		return -ERESTARTSYS;

	minor = MINOR(filp->f_dentry->d_inode->i_rdev);

	ret = buf_write(minor, buf, count);

	up(&ssc_mutex);

	return ret;
}

long ssc_ioctl(struct file *filp, unsigned int ioctl_num, unsigned long ioctl_param)
{
	int minor;
	SSC_IOCTL_PARAM param;

	if(down_interruptible(&ssc_mutex))
		return -ERESTARTSYS;

	minor = MINOR(filp->f_dentry->d_inode->i_rdev);

	copy_from_user((char*)(&param), (char*)ioctl_param, sizeof(param));
#ifdef	_SSC_DEBUG_
	printk("<1>SSC:IOCTL %d\n", param.cmd);
#endif

	if(param.signature != SSC_IOCTL_SIG)
		goto exit;

	switch(param.cmd) {
	case SSC_IOCTL_CMD_INIT:
		param.param[MAX_FILE_NAME -1] = 0;
		ssc_dev_open(minor, param.param);
		break;
	case SSC_IOCTL_CMD_FLUSH:
		buf_flush(minor);
		break;
	case SSC_IOCTL_CMD_INFO:
		ssc_dev_info(minor, ioctl_param);
		break;
	}

exit:
	up(&ssc_mutex);
	return 0;
}
