#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<fcntl.h>
#include<sys/ioctl.h>
#include "conf.h"
#include "ssc.h"

#define	IOCTL_SET_MSG	_IOR(SSC_MAJOR, 0, char*)

int 
main(int argc, char** argv)
{
	if(argc<2) {
		printf("USAGE: %s <device> <file name>\n", argv[0]);
	} else {
		int fp;
		SSC_IOCTL_PARAM param;

		param.signature = SSC_IOCTL_SIG;
		param.cmd = SSC_IOCTL_CMD_INIT;
		if(argc>=3)
			strcpy(param.param, argv[2]);
		else
			param.param[0] = '\0';

		fp = open(argv[1], 0);
		ioctl(fp, IOCTL_SET_MSG, &param);

		printf("%s has been initialized as %s\n", argv[1], argv[2]);
	}

	return 0;
}
