#ifndef	__SSC_H__
#define	__SSC_H__

#define	MAX_FILE_NUM	16
#define	MAX_FILE_NAME	256

#define	SSC_PAGE_SIZE	(32 * 1024)
#define	PIPE_TO_DEV	(SSC_PAGE_SIZE*1024)
#define	PIPE_FROM_DEV	(SSC_PAGE_SIZE*2048)
#define	PIPE_END	(SSC_PAGE_SIZE*3072)
#define	OFFSET_WDATA	4
#define	OFFSET_RDATA	8
#define	MARGIN_WDATA	512
#define	BUFCHECK_LEN	128

#define	MSG_CREATE	0x00
#define	MSG_READ	0x01
#define	MSG_APPEND	0x02
#define	MSG_INFO	0x03
#define	MSG_ACK		0x10

typedef struct {
	int signature;
	int cmd;
	char param[MAX_FILE_NAME + 9*4];
} SSC_IOCTL_PARAM;

#define	SSC_IOCTL_SIG		0x1234abcd
#define	SSC_IOCTL_CMD_INIT	0
#define	SSC_IOCTL_CMD_FLUSH	1
#define	SSC_IOCTL_CMD_INFO	2

typedef struct {
	unsigned int id;
	unsigned int st_pos;
	char name[MAX_FILE_NAME];

	unsigned int end_pos;
	unsigned int cur_pos;
	unsigned int cur_bytes;
	unsigned int prev_size;
	unsigned int total_size;
	unsigned int actual_size;
	unsigned int mark_pos;
} SSC_DEV_INFO_DESC_T;

#endif
