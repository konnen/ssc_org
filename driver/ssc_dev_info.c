#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<fcntl.h>
#include<sys/ioctl.h>
#include "conf.h"
#include "ssc.h"

#define	IOCTL_SET_MSG	_IOR(SSC_MAJOR, 0, char*)

int 
main(int argc, char** argv)
{
	if(argc<2) {
		printf("USAGE: %s <device>\n", argv[0]);
	} else {
		int fp;
		SSC_IOCTL_PARAM param;
		SSC_DEV_INFO_DESC_T* ssc_dev_info_desc;

		param.signature = SSC_IOCTL_SIG;
		param.cmd = SSC_IOCTL_CMD_INFO;

		fp = open(argv[1], 0);
		ioctl(fp, IOCTL_SET_MSG, &param);
		ssc_dev_info_desc = (SSC_DEV_INFO_DESC_T*)param.param;

		printf("Minor %d (%s): %d bytes\n"
			, ssc_dev_info_desc->id
			, ssc_dev_info_desc->name
			, ssc_dev_info_desc->actual_size
		);
	}

	return 0;
}
