#!/usr/bin/perl
use Time::HiRes qw(gettimeofday);

#######################################################
# Configuration
#######################################################

$MAJOR = 60;
$SSC_INIT = "../ssc/ssc_init_file";


#######################################################
# Initialization
#######################################################

if($#ARGV<0) {
	printf("USAGE: ./test.pl <minor>\n");
	exit;
}

$MINOR = $ARGV[0];
#$NUM_CHECK_POINT = 100;
$NUM_CHECK_POINT = 10;
#$MAX_ITER = 10 * 1024 * 1024 * 1024 / 40;
$MAX_ITER = 1000;
$ITER_PER_CP = $MAX_ITER / $NUM_CHECK_POINT;
$SLEEP_INTV = 1;

$SSC_DEV = "/dev/ssc$MINOR";
$LOG_FILE_PATH = "log$MINOR";
$COMP_FILE_SSC = "comp_ssc_$MINOR";
$COMP_FILE_LOG = "comp_log_$MINOR";
$RESULT_PATH = "result$MINOR";
$TEST_LOG_STRING = "This is a test log string";
$TEMP_FILE = "temp";

sub kill_tail_proc
{
	my @ps_fields;
	system("ps -ef | grep tail | grep $LOG_FILE_PATH > temp");
	open TAIL, $TEMP_FILE;
	@ps_fields = split(/\s+/, <TAIL>);
	close TAIL;
	unlink $TEMP_FILE;
	if($ps_fields[1] > 0) {
		kill 9, $ps_fields[1];
	}
}

unless (-e $SSC_DEV) {
	system("mknod $SSC_DEV c $MAJOR $MINOR") && die("Cannot mknod $SSC_DEV: $!");
	system("$SSC_INIT $SSC_DEV TEST$MINOR") && die("Cannot init dev $SSC_DEV: $!");
}

if(-e $LOG_FILE_PATH) {
	unlink $LOG_FILE_PATH || die("Cannot remove $LOG_FILE_PATH: $!");
}

open(LOG_FILE, ">$LOG_FILE_PATH") || die("Cannot open $LOG_FILE_PATH: $!");
close(LOG_FILE);

&kill_tail_proc;
#system("tail -F $LOG_FILE_PATH > $SSC_DEV &");

if(-e $RESULT_PATH) {
	system("rm -rf $RESULT_PATH") && die("Cannot remove $RESULT_PATH: $!");
}

mkdir $RESULT_PATH || die("Cannot mkdir $RESULT_PATH: $!");


#######################################################
# Stress test
#######################################################

$total_acc_avg = 0.0;
$total_acc_cnt = 0;

$| = 1;
for($cp = 0; $cp < $NUM_CHECK_POINT; $cp++)
{
	print ("\n====== Check Point $cp ======\n");

	$cp_acc_avg = 0.0;
	$cp_acc_cnt = 0;
	for($it = 0; $it < $ITER_PER_CP; $it++) {
		if(int($it*100/$ITER_PER_CP) != int(($it-1)*100/$ITER_PER_CP)) {
			printf("Generating log... %02d %\r", int(($it+1)/($ITER_PER_CP/100)));
		}
		my($sec, $min, $hr, $day, $mon, $year) = localtime;
		my($s, $usec) = gettimeofday();
		my($len, $str);
		my($line);
		my($s_start, $s_end);
		my($us_start, $us_end);
		my($us_acc);
		$len = length $TEST_LOG_STRING;
		$str = substr $TEST_LOG_STRING, 0, int(rand($len-1))+1;
		$line = sprintf("%02d/%02d/%04d %02d:%02d:%02d.%06d $str\n", $day, $mon+1, $year+1900, $hr, $min, $sec, $usec);
		if($SLEEP_INTV>0) {
			sleep($SLEEP_INTV);
		}
		($s_start, $us_start) = gettimeofday();
		open(LOG_FILE, ">>$LOG_FILE_PATH") || die("Cannot open $LOG_FILE_PATH: $!");
		print LOG_FILE $line;
		close(LOG_FILE);
		#system("echo $line >> $LOG_FILE_PATH");
		#system("echo $line > $SSC_DEV");
		#sysopen(FP_SSC, "$SSC_DEV", O_RDWR | O_SYNC) || die("Cannot open $SSC_DEV: $!");
		open(FP_SSC, ">$SSC_DEV") || die("Cannot open $SSC_DEV: $!");
		print FP_SSC $line;
		close(FP_SSC);
		($s_end, $us_end) = gettimeofday();
		$us_acc = $s_end*1000000+$us_end - $s_start*1000000-$us_start;

		$cp_acc_avg = ( $cp_acc_avg * $cp_acc_cnt + $us_acc ) / ( $cp_acc_cnt + 1 );
		$cp_acc_cnt++;
		$total_acc_avg = ( $total_acc_avg * $total_acc_cnt + $us_acc ) / ( $total_acc_cnt + 1 );
		$total_acc_cnt++;
	}

	print ("Generating log... 100 %\n\n");
	system("sync");
	if($SLEEP_INTV>0) {
		sleep($SLEEP_INTV);
	}
	print ("Average access time = $cp_acc_avg usec\n");
	print ("Cumulative average access time = $total_acc_avg usec\n");

	print ("Reading log from SSC device...\n");
	#system("$SSC_INIT $SSC_DEV TEST$MINOR") && die("Cannot init dev $SSC_DEV: $!");
	if($SLEEP_INTV>0) {
		sleep($SLEEP_INTV);
	}
	system("cat $SSC_DEV > $COMP_FILE_SSC");
	system("wc -l $COMP_FILE_SSC > $TEMP_FILE");
	open WC, $TEMP_FILE;
	$_ = <WC>;
	@line_split = split(/ /, $_);
	$lines = $line_split[0];
	close WC;
	unlink $TEMP_FILE;

	print ("Reading last $lines lines from log file...\n");
	system("tail -n $lines $LOG_FILE_PATH > $COMP_FILE_LOG");

	print ("Comparing results...\n");
	system("diff $COMP_FILE_SSC $COMP_FILE_LOG > $RESULT_PATH/cp$cp");

	my $filesize = -s "$RESULT_PATH/cp$cp";
	if($filesize==0) {
		print ("CHECK POINT $cp OK\n");
	} else {
		print ("CHECK POINT $cp FAIL\n");
		#exit;
	}

	unlink $COMP_FILE_SSC;
	system("mv $COMP_FILE_LOG $LOG_FILE_PATH");
}

&kill_tail_proc;
